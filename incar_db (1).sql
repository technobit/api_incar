-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2020 at 02:25 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `incar_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getDesa` (IN `in_intIDKecamatan` INT)  BEGIN
 select intIDKecamatan,intIDDesa,txtDesa from m_desa where intIDKecamatan = in_intIDKecamatan order by txtDesa asc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getKecamatan` ()  BEGIN
 select intIDKecamatan,txtKecamatan from m_kecamatan order by txtKecamatan asc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_delHistorySurveyBangunan` (IN `in_intIDSurveyBangunan` INT)  BEGIN
  delete from  tr_survey_bangunan where intIDSurveyBangunan = in_intIDSurveyBangunan;
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_delHistorySurveyCurahHujan` (IN `in_intIDCurahHujan` INT)  BEGIN
  delete from  tr_surveycurahhujan where intIDCurahHujan = in_intIDCurahHujan;
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_delHistorySurveyKerusakanTanaman` (IN `in_intIDSurveyKerusakanTanaman` INT)  BEGIN
delete from  tr_surveykerusakantanaman where intIDSurveyKerusakanTanaman = in_intIDSurveyKerusakanTanaman;
Delete from tr_tanamanrusak where intIDSurveyKerusakanTanaman = in_intIDSurveyKerusakanTanaman;
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_delHistorySurveyPintuAir` (IN `in_intIDSurveyPintuAir` INT)  BEGIN
  delete from  tr_surveypintuair where intIDSurveyPintuAir = in_intIDSurveyPintuAir;
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_delHistorySurveyRealisasiTanaman` (IN `in_intIDSurveyRealisasiTanaman` INT)  BEGIN	
  delete from  tr_surveyrealisasitanaman where intIDSurveyRealisasiTanaman = in_intIDSurveyRealisasiTanaman;
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDataBangunan` ()  BEGIN
 select B.intIDBangunan,B.txtBangunan,
(SELECT J.txtJenisSaluranBangunan from m_jenissaluran_bangunan J where J.intIDJenisSaluranBangunan = B.intIDJenisSaluranBangunan) as txtJenisSaluranBangunan,
B.dblLatitude, B.dblLongitude, B.intIDJaringanIrigasi, B.intIDJenisSaluranBangunan
from m_bangunan_irigasi B;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDataJaringanIrigasi` (IN `in_intIDUser` INT)  BEGIN
 select intIDJaringanIrigasi,txtJaringanIrigasi from m_jaringanirigasi where intIDJaringanIrigasi in (select intIDJaringanIrigasi from m_user_akses_jaringanirigasi where intIDUser = in_intIDUser)
order by txtJaringanIrigasi ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDataStasiunCurahHujan` (IN `in_intIDUser` INT)  BEGIN
 select intIDKecamatan, intIDStasiunCurahHujan,txtStasiunCurahHujan,txtNo,txtElevasi,dblLatitude,dblLongitude,isAreaStrict from m_stasiuncurahhujan where intIDStasiunCurahHujan in (select intIDStasiunCurahHujan from m_user_akses_stasiuncurahhujan where intIDUser = in_intIDUser);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailHistorySurveyCurahHujan` (IN `in_intIDCurahHujan` INT)  BEGIN
  select SH.intIDCurahHujan,SH.intIDKecamatan,
	 (select K.txtKecamatan from m_kecamatan K where K.intIDKecamatan = SH.intIDKecamatan limit 1) as txtKecamatan,
	SH.intIDStasiunCurahHujan,SH.dblCurahHujan,SH.dblLatitude,SH.dblLongitude,SH.dtSurvey,
(select CH.txtNo from m_stasiuncurahhujan CH where CH.intIDStasiunCurahHujan = SH.intIDStasiunCurahHujan) txtStasiunNo,
(select CH.txtElevasi from m_stasiuncurahhujan CH where CH.intIDStasiunCurahHujan = SH.intIDStasiunCurahHujan) txtStasiunElevasi,
(select CH.dblLatitude from m_stasiuncurahhujan CH where CH.intIDStasiunCurahHujan = SH.intIDStasiunCurahHujan) dblStasiunLatitude,
(select CH.dblLongitude from m_stasiuncurahhujan CH where CH.intIDStasiunCurahHujan = SH.intIDStasiunCurahHujan) dblStasiunLongitude
from  tr_surveycurahhujan SH where SH.intIDCurahHujan = in_intIDCurahHujan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailSurveyBangunan` (IN `in_intIDSurveyBangunan` INT)  BEGIN


select TR.intIDSurveyBangunan,TR.intIDBangunan,
(select B.txtBangunan from m_bangunan_irigasi B where B.intIDBangunan = TR.intIDBangunan) as txtBangunan,
TR.intIDJaringanIrigasi,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi, TR.intIDJenisSaluran,
(select JS.txtJenisSaluranBangunan from m_jenissaluran_bangunan JS where JS.intIDJenisSaluranBangunan = TR.intIDJenisSaluran) as txtJenisSaluran, TR.dblLatitude, TR.dblLongitude,
TR.intKondisi, TR.intFungsi,TR.intMempengaruhiAliranAir,
TR.txtFoto1,TR.txtFoto2,TR.txtFoto3,TR.txtFoto4,TR.txtFoto5, TR.txtKeterangan, TR.dtSurvey
from tr_survey_bangunan TR
where TR.intIDSurveyBangunan = in_intIDSurveyBangunan;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailSurveyJenisTanamanRusak` (IN `in_intIDSurveyKerusakanTanaman` INT)  BEGIN
select TR.intIDJenisTanaman, TR.intIDTanamanRusak,TR.intIDSurveyKerusakanTanaman,
(select J.txtJenisTanaman from m_jenistanaman J where J.intIDJenisTanaman = TR.intIDJenisTanaman) as txtJenisTanaman
from tr_tanamanrusak TR where TR.intIDSurveyKerusakanTanaman = in_intIDSurveyKerusakanTanaman;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailSurveyKerusakanTanaman` (IN `in_intIDSurveyKerusakanTanaman` INT)  BEGIN
select A.dtSurvey,A.intIDSaluran, A.intIDJaringanIrigasi,A.txtJaringanIrigasi,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = A.intIDSaluran) as txtNamaSaluran,
A.intIDJenisSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = A.intIDJenisSaluran) as txtJenisSaluran,
A.dblBakuSawah, A.intIDKecamatan,
(select K.txtKecamatan from m_kecamatan K where K.intIDKecamatan = A.intIDKecamatan) as txtKecamatan,
A.intIDDesa,
(select D.txtDesa from m_desa D where D.intIDKecamatan = A.intIDKecamatan AND D.intIDDesa = A.intIDDesa) as txtDesa,
A.intIDProgramIntensifikasi,
(select P.txtProgramIntensifikasi from m_programintensifikasi P where P.intIDProgramIntensifikasi = A.intIDProgramIntensifikasi) as txtProgramIntensifikasi,
A.intIDPenyebabKerusakan,
(select P.txtPenyebabKerusakan from m_penyebabkerusakan P where P.intIDPenyebabKerusakan = A.intIDPenyebabKerusakan) as txtPenyebabKerusakan, A.dblMati, A.dblPuso,A.dblBerat,A.dblSedang,A.dblRingan,A.dblLamaGenangan,A.txtKeterangan
from
(SELECT TR.*,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran ) as intIDJenisSaluran
FROM tr_surveykerusakantanaman TR) A where A.intIDSurveyKerusakanTanaman = in_intIDSurveyKerusakanTanaman;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailSurveyPintuAir` (IN `in_intIDSurveyPintuAir` INT)  BEGIN
select A.dtSurvey,A.intIDJaringanIrigasi,A.txtJaringanIrigasi, A.intIDSaluran,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = A.intIDSaluran) as txtNamaSaluran,
A.intIDJenisSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = A.intIDJenisSaluran) as txtJenisSaluran,
A.dblBakuSawah, A.Pelaksanaan_dblDebit, A.Pelaksanaan_dblLPR, A.Pelaksanaan_dblFPR,
A.Perencanaan_dblDebit, A.Perencanaan_dblLPR, A.Perencanaan_dblFPR
from
(SELECT TR.*,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran) as intIDJenisSaluran
FROM tr_surveypintuair TR) A where A.intIDSurveyPintuAir = in_intIDSurveyPintuAir;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getDetailSurveyRealisasiTanaman` (IN `in_intIDSurveyRealisasiTanaman` INT)  BEGIN
select A.*,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = A.intIDSaluran) as txtNamaSaluran,
A.intIDJenisSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = A.intIDJenisSaluran) as txtJenisSaluran
from
(SELECT TR.*,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran ) as intIDJenisSaluran
FROM tr_surveyrealisasitanaman TR) A where A.intIDSurveyRealisasiTanaman = in_intIDSurveyRealisasiTanaman;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getHistorySurveyBangunan` (IN `in_limit` INT, IN `in_offset` INT, IN `in_intIDUser` INT)  BEGIN


Select TR.intIDSurveyBangunan, TR.dtSurvey,TR.intIDJaringanIrigasi,
(select JS.txtBangunan from m_bangunan_irigasi JS where JS.intIDBangunan = TR.intIDBangunan) as txtBangunan,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select JS.txtJenisSaluranBangunan from m_jenissaluran_bangunan JS where JS.intIDJenisSaluranBangunan = TR.intIDJenisSaluran) as txtJenisSaluranBangunan from tr_survey_bangunan TR where TR.intIDUser = in_intIDUser  ORDER BY TR.dtSurvey DESC LIMIT in_limit OFFSET in_offset; 




END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getHistorySurveyCurahHujan` (IN `in_limit` INT, IN `in_offset` INT, IN `in_intIDUser` INT)  BEGIN
 SELECT CH.intIDCurahHujan,CH.intIDKecamatan,
 (select K.txtKecamatan from m_kecamatan K where K.intIDKecamatan = CH.intIDKecamatan limit 1) as txtKecamatan,
 CH.intIDStasiunCurahHujan,
 (select S.txtStasiunCurahHujan from m_stasiuncurahhujan S where S.intIDStasiunCurahHujan = CH.intIDStasiunCurahHujan limit 1) as txtStasiunCurahHujan,
 CH.dblCurahHujan,CH.dtSurvey FROM tr_surveycurahhujan CH Where CH.intIDUser = in_intIDUser 
order by CH.dtSurvey DESC LIMIT in_limit OFFSET in_offset; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getHistorySurveyKerusakanTanaman` (IN `in_limit` INT, IN `in_offset` INT, IN `in_intIDUser` INT)  BEGIN
select X.dtSurvey,X.intIDSurveyKerusakanTanaman,X.txtJaringanIrigasi ,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = X.intIDSaluran ) as txtSaluran,
X.intIDJenisSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = X.intIDJenisSaluran) as txtJenisSaluran
from
( select TR.intIDSaluran,TR.dtSurvey,TR.intIDSurveyKerusakanTanaman,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran ) as intIDJenisSaluran
FROM tr_surveykerusakantanaman TR where TR.intIDUser = in_intIDUser
order by TR.dtSurvey DESC LIMIT in_limit OFFSET in_offset ) X; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getHistorySurveyPintuAir` (IN `in_limit` INT, IN `in_offset` INT, IN `in_intIDUser` INT)  BEGIN
select X.dtSurvey,X.intIDSurveyPintuAir,X.txtJaringanIrigasi ,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = X.intIDJenisSaluran ) as txtSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = X.intIDJenisSaluran) as txtJenisSaluran
from
( select TR.dtSurvey,TR.intIDSurveyPintuAir,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran ) as intIDJenisSaluran
FROM tr_surveypintuair TR Where TR.intIDUser = in_intIDUser
order by TR.dtSurvey DESC LIMIT in_limit OFFSET in_offset ) X; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getHistorySurveyRealisasiTanaman` (IN `in_limit` INT, IN `in_offset` INT, IN `in_intIDUser` INT)  BEGIN
select X.dtSurvey,X.intIDSurveyRealisasiTanaman,X.txtJaringanIrigasi ,
(select S.txtSaluran from m_saluran S where S.intIDSaluran = X.intIDJenisSaluran ) as txtSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = X.intIDJenisSaluran) as txtJenisSaluran
from
( select TR.dtSurvey,TR.intIDSurveyRealisasiTanaman,
(Select JG.txtJaringanIrigasi from m_jaringanirigasi JG where JG.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran ) as intIDJenisSaluran
FROM tr_surveyrealisasitanaman TR where TR.intIDUser = in_intIDUser
order by TR.dtSurvey DESC LIMIT in_limit OFFSET in_offset ) X; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getJenisSaluran` ()  BEGIN
 select intIDJenisSaluran,txtJenisSaluran from m_jenissaluran;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getJenisTanaman` ()  BEGIN
 select intiDJenisTanaman,txtJenisTanaman from m_jenistanaman;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getPenyebabKerusakan` ()  BEGIN
 select intIDPenyebabKerusakan,txtPenyebabKerusakan from m_penyebabkerusakan order by txtPenyebabKerusakan ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getProgramIntensifikasi` ()  BEGIN
 select intIDProgramIntensifikasi,txtProgramIntensifikasi from m_programintensifikasi order by txtProgramIntensifikasi ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSaluran` ()  BEGIN
 SELECT COUNT(intIDSaluran) as intRowCount FROM m_saluran;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSurveyBangunan` (IN `in_intIDUser` INT)  BEGIN
 SELECT COUNT(intIDSurveyBangunan) as intRowCount FROM tr_survey_bangunan where intIDUser = in_intIDUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSurveyCurahHujan` (IN `in_intIDUser` INT)  BEGIN
 SELECT COUNT(intIDCurahHujan) as intRowCount FROM tr_surveycurahhujan where intIDUser = in_intIDUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSurveyKerusakanTanaman` (IN `in_intIDUser` INT)  BEGIN
 SELECT COUNT(intIDSurveyKerusakanTanaman) as intRowCount FROM tr_surveykerusakantanaman where intIDUser = in_intIDUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSurveyPintuAir` (IN `in_intIDUser` INT)  BEGIN
 SELECT COUNT(intIDSurveyPintuAir) as intRowCount FROM tr_surveypintuair where intIDUser = in_intIDUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getRowCountDataSurveyRealisasiTanaman` (IN `in_intIDUser` INT)  BEGIN
 SELECT COUNT(intIDSurveyRealisasiTanaman) as intRowCount FROM tr_surveyrealisasitanaman where intIDUser = in_intIDUser;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getSaluran` (IN `in_intIDJaringanIrigasi` INT, IN `in_intIDJenisSaluran` INT)  BEGIN
 select S.intIDJaringanIrigasi, S.intIDSaluran,S.txtSaluran,S.intIDJenisSaluran as IntIDJenisSaluranSurvey,
(SELECT J.txtJenisSaluran from m_jenissaluran J where J.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran,
S.dblBakuSawah
from m_saluran S where S.intIDJaringanIrigasi = in_intIDJaringanIrigasi AND S.intIDJenisSaluran = in_intIDJenisSaluran;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getSaluranPrimer` ()  BEGIN
 select S.intIDJaringanIrigasi, S.intIDSaluran,S.txtSaluran,S.intIDJenisSaluran as IntIDJenisSaluranSurvey,
(SELECT J.txtJenisSaluran from m_jenissaluran J where J.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran
from m_saluran S where  S.intIDJenisSaluran = 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getSaluranSekunder` ()  BEGIN
  select S.intIDJaringanIrigasi, S.intIDSaluran,S.txtSaluran,S.intIDJenisSaluran as IntIDJenisSaluranSurvey,
(SELECT J.txtJenisSaluran from m_jenissaluran J where J.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran
from m_saluran S where  S.intIDJenisSaluran = 2;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_getSaluranTersier` ()  BEGIN
  select S.intIDJaringanIrigasi, S.intIDSaluran,S.txtSaluran,S.intIDJenisSaluran as IntIDJenisSaluranSurvey,
(SELECT J.txtJenisSaluran from m_jenissaluran J where J.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran
from m_saluran S where S.intIDJenisSaluran = 3;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_saluran_delete` (IN `in_intIDSaluran` INT)  BEGIN
 
 delete from m_saluran where intIDSaluran = in_intIDSaluran;
 
 select 1 as bitSuccess;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_saluran_detail` (IN `in_intIDSaluran` INT)  BEGIN
 
 select S.intIDSaluran,S.intIDJaringanIrigasi,
 (SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = S.intIDJaringanIrigasi) txtJaringanIrigasi,
 S.txtKodeSingkatan,S.txtSaluran,S.intIDJenisSaluran,
 (select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran, S.intIDSaluranAsal,
 (SELECT SS.txtSaluran from m_saluran SS where SS.intIDSaluran = S.intIDSaluranAsal) txtSaluranAsal
 from m_saluran S where S.intIDSaluran = in_intIDSaluran;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_saluran_insert` (IN `in_intIDJaringanIrigasi` INT, IN `in_txtKodeSingkatan` VARCHAR(50), IN `in_txtSaluran` VARCHAR(50), IN `in_intIDJenisSaluran` INT, IN `in_intIDSaluranAsal` INT)  BEGIN
 
 IF (SELECT count(intIDSaluran) FROM m_saluran where intIDJaringanIrigasi = in_intIDJaringanIrigasi and  txtSaluran = in_txtSaluran)>0 THEN
    BEGIN
			 select 0 as bitSuccess, 'Nama Saluran Sudah Ada' as txtInfo;
    END;
    ELSE 
    BEGIN
     INSERT INTO m_saluran( `intIDJaringanIrigasi`, `txtKodeSingkatan`, `txtSaluran`, `intIDJenisSaluran`, `intIDSaluranAsal`) VALUES (in_intIDJaringanIrigasi, in_txtKodeSingkatan, in_txtSaluran, in_intIDJenisSaluran, in_intIDSaluranAsal);
		 
		  select 1 as bitSuccess,'Success' as txtInfo;
		 
    END;
    END IF;
 
 
 
 

 
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_saluran_retrieve` (IN `in_limit` INT, IN `in_offset` INT)  BEGIN
 
 select S.intIDSaluran,S.intIDJaringanIrigasi,
 (SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = S.intIDJaringanIrigasi) txtJaringanIrigasi,
 S.txtKodeSingkatan,S.txtSaluran,S.intIDJenisSaluran,
 (select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = S.intIDJenisSaluran) txtJenisSaluran, S.intIDSaluranAsal,
 (SELECT SS.txtSaluran from m_saluran SS where SS.intIDSaluran = S.intIDSaluranAsal) txtSaluranAsal
 from m_saluran S LIMIT in_limit OFFSET in_offset;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_saluran_update` (IN `in_intIDSaluran` INT, IN `in_intIDJaringanIrigasi` INT, IN `in_txtKodeSingkatan` VARCHAR(50), IN `in_txtSaluran` VARCHAR(50), IN `in_intIDJenisSaluran` INT, IN `in_intIDSaluranAsal` INT)  BEGIN
 
UPDATE m_saluran SET `intIDJaringanIrigasi` = in_intIDJaringanIrigasi, `txtKodeSingkatan` = in_txtKodeSingkatan, `txtSaluran` = in_txtSaluran, `intIDJenisSaluran` = in_intIDJenisSaluran, `intIDSaluranAsal` = in_intIDSaluranAsal WHERE `intIDSaluran` = in_intIDSaluran;
 
 select 1 as bitSuccess;
 
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveybangunan_insert` (IN `in_intIDJaringanIrigasi` INT, IN `in_intIDBangunan` INT, IN `in_intIDJenisSaluran` INT, IN `in_dblLatitude` DOUBLE(11,8), IN `in_dblLongitude` DOUBLE(11,8), IN `in_intKondisi` INT, IN `in_intFungsi` INT, IN `in_intMempengaruhiAliranAir` INT, IN `in_txtFoto1` TEXT, IN `in_txtFoto2` TEXT, IN `in_txtFoto3` TEXT, IN `in_txtFoto4` TEXT, IN `in_txtFoto5` TEXT, IN `in_txtKeterangan` VARCHAR(200), IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN

 INSERT INTO tr_survey_bangunan(
 `intIDJaringanIrigasi`,
 `intIDBangunan`,
 `intIDJenisSaluran`,
 `dblLatitude`,
 `dblLongitude`,
 `intKondisi`,
 `intFungsi`,
 `intMempengaruhiAliranAir`,
 `txtFoto1`,
 `txtFoto2`, 
 `txtFoto3`,
 `txtFoto4`,
 `txtFoto5`,
 `txtKeterangan`, 
 `dtSurvey`, 
 `intIDUser`)
 VALUES 
 ( in_intIDJaringanIrigasi,
 in_intIDBangunan,
 in_intIDJenisSaluran,
 in_dblLatitude,
 in_dblLongitude,
 in_intKondisi,
 in_intFungsi,
 in_intMempengaruhiAliranAir,
 in_txtFoto1,
 in_txtFoto2,
 in_txtFoto3,
 in_txtFoto4,
 in_txtFoto5,
 in_txtKeterangan,
 in_dtSurvey,
 in_intIDUser);
 
 select 1 as bitSuccess;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveybangunan_update_insert_foto` (IN `in_intIDSurveyBangunan` INT, IN `in_txtFoto1` TEXT, IN `in_txtFoto2` TEXT, IN `in_txtFoto3` TEXT, IN `in_txtFoto4` TEXT, IN `in_txtFoto5` TEXT)  BEGIN

update tr_survey_bangunan set txtFoto1 = in_txtFoto1,
txtFoto2 = in_txtFoto2,
txtFoto3 = in_txtFoto3,
txtFoto4 = in_txtFoto4,
txtFoto5 = in_txtFoto5 
where intIDSurveyBangunan = in_intIDSurveyBangunan;

select 1 as bitSuccess;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveycurahhujan_insert` (IN `in_intIDKecamatan` INT, IN `in_intIDStasiunCurahHujan` INT, IN `in_dblCurahHujan` DOUBLE, IN `in_dblLatitude` DOUBLE(11,8), IN `in_dblLongitude` DOUBLE(11,8), IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN
 INSERT INTO `tr_surveycurahhujan`
 (`intIDKecamatan`,
 `intIDStasiunCurahHujan`,
 `dblCurahHujan`,
 `dblLatitude`,
 `dblLongitude`,
 `dtSurvey`,
 intIDuser)
 VALUES 
 (in_intIDKecamatan,
 in_intIDStasiunCurahHujan,
 in_dblCurahHujan,
 in_dblLatitude,
 in_dblLongitude,
 in_dtSurvey,
 in_intIDUser
 );
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveyjenistanamanrusak_insert` (IN `in_intIDSurveyKerusakanTanaman` INT, IN `in_intIDJenisTanaman` INT)  BEGIN
INSERT INTO `tr_tanamanrusak`(
`intIDSurveyKerusakanTanaman`,
`intIDJenisTanaman`)
VALUES
( in_intIDSurveyKerusakanTanaman, in_intIDJenisTanaman);
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveykerusakantanaman_insert` (IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_intIDKecamatan` INT, IN `in_intIDDesa` BIGINT, IN `in_intIDProgramIntensifikasi` INT, IN `in_intIDPenyebabKerusakan` INT, IN `in_dblMati` DOUBLE, IN `in_dblPuso` DOUBLE, IN `in_dblBerat` DOUBLE, IN `in_dblSedang` DOUBLE, IN `in_dblRingan` DOUBLE, IN `in_dblLamaGenangan` DOUBLE, IN `in_txtKeterangan` VARCHAR(100), IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN
INSERT INTO `tr_surveykerusakantanaman`(
intIDJaringanIrigasi,
intIDSaluran,
`dblBakuSawah`,
`intIDKecamatan`,
`intIDDesa`,
`intIDProgramIntensifikasi`, 
`intIDPenyebabKerusakan`,
`dblMati`,
`dblPuso`,
`dblBerat`,
`dblSedang`,
`dblRingan`,
`dblLamaGenangan`,
`txtKeterangan`,
dtSurvey,
intIDUser
)
VALUES 
(in_intIDJaringanIrigasi,
in_intIDSaluran,
in_dblBakuSawah,
in_intIDKecamatan,
in_intIDDesa,
in_intIDProgramIntensifikasi,
in_intIDPenyebabKerusakan,
in_dblMati,
in_dblPuso,
in_dblBerat,
in_dblSedang,
in_dblRingan, 
in_dblLamaGenangan,
in_txtKeterangan,
in_dtSurvey,
in_intIDUser
);
 
 SELECT 1 as bitSuccess, LAST_INSERT_ID() as intIDSurveyKerusakanTanaman;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveypintuair_insert` (IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_Pelaksanaan_dblDebit` DOUBLE, IN `in_Pelaksanaan_dblLPR` DOUBLE, IN `in_Pelaksanaan_dblFPR` DOUBLE, IN `in_Perencanaan_dblDebit` DOUBLE, IN `in_Perencanaan_dblLPR` DOUBLE, IN `in_Perencanaan_dblFPR` DOUBLE, IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN
INSERT INTO `tr_surveypintuair`( 
`intIDJaringanIrigasi`,
intIDSaluran,
dblBakuSawah,
`Pelaksanaan_dblDebit`,
`Pelaksanaan_dblLPR`,
`Pelaksanaan_dblFPR`, 
`Perencanaan_dblDebit`,
`Perencanaan_dblLPR`,
`Perencanaan_dblFPR`,
`dtSurvey`,
intIDuser
) 
VALUES 
(
in_intIDJaringanIrigasi,
in_intIDSaluran,
in_dblBakuSawah,
in_Pelaksanaan_dblDebit,
in_Pelaksanaan_dblLPR,
in_Pelaksanaan_dblFPR,
in_Perencanaan_dblDebit,
in_Perencanaan_dblLPR,
in_Perencanaan_dblFPR,
in_dtSurvey,
in_intIDUser
);
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_surveyrealisasitanaman_insert` (IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_txtKeterangan` VARCHAR(150), IN `in_Debit_Intake` DOUBLE, IN `in_IjinAir` DOUBLE, IN `in_Debit_Efisiensi` DOUBLE, IN `in_TotalTersier` DOUBLE, IN `in_dblLPR` DOUBLE, IN `in_dblFPR` DOUBLE, IN `in_Padi_MH_dblBibit` DOUBLE, IN `in_Padi_MH_dblGarap` DOUBLE, IN `in_Padi_MH_dblTanam` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblBibit` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblGarap` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblTanam` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblBibit` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblGarap` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblTanam` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblBibit` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblGarap` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblTanam` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblBibit` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblGarap` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblTanam` DOUBLE, IN `in_Padi_Total` DOUBLE, IN `in_TebuRakyat_dblCemplong` DOUBLE, IN `in_TebuRakyat_dblBibit` DOUBLE, IN `in_TebuRakyat_dblMuda` DOUBLE, IN `in_TebuRakyat_dblTua` DOUBLE, IN `in_TebuRakyat_dblTotal` DOUBLE, IN `in_TebuPerusahaan_dblCemplong` DOUBLE, IN `in_TebuPerusahaan_dblBibit` DOUBLE, IN `in_TebuPerusahaan_dblMuda` DOUBLE, IN `in_TebuPerusahaan_dblTua` DOUBLE, IN `in_TebuPerusahaan_dblTotal` DOUBLE, IN `in_Polowijo_MH` DOUBLE, IN `in_Polowijo_MK1_Jagung` DOUBLE, IN `in_Polowijo_MK1_Kedelai` DOUBLE, IN `in_Polowijo_MK1_Lainnya` DOUBLE, IN `in_Polowijo_MK2_Jagung` DOUBLE, IN `in_Polowijo_MK2_Kedelai` DOUBLE, IN `in_Polowijo_MK2_Lainnya` DOUBLE, IN `in_Polowijo_Tembakau` DOUBLE, IN `in_Polowijo_Rosela` DOUBLE, IN `in_Polowijo_Total` DOUBLE, IN `in_Bero_dblAsli` DOUBLE, IN `in_Bero_dblPascaPanen` DOUBLE, IN `in_Bero_Total` DOUBLE, IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN
INSERT INTO `tr_surveyrealisasitanaman`(
`intIDJaringanIrigasi`,
`intIDSaluran`,
`dblBakuSawah`,
 txtKeterangan,
 Debit_Intake,
 Debit_IjinAir,
 Debit_Efisiensi,
 Debit_TotalTersier,
 `dblLPR`,
`dblFPR`, 
`Padi_MH_dblBibit`,
`Padi_MH_dblGarap`,
`Padi_MH_dblTanam`,
`Padi_MK1_GaduIjin_dblBibit`,
`Padi_MK1_GaduIjin_dblGarap`,
`Padi_MK1_GaduIjin_dblTanam`,
`Padi_MK1_GaduTidakIjin_dblBibit`, 
`Padi_MK1_GaduTidakIjin_dblGarap`,
`Padi_MK1_GaduTidakIjin_dblTanam`,
`Padi_MK2_GaduIjin_dblBibit`,
`Padi_MK2_GaduIjin_dblGarap`,
`Padi_MK2_GaduIjin_dblTanam`,
`Padi_MK2_GaduTidakIjin_dblBibit`,
`Padi_MK2_GaduTidakIjin_dblGarap`,
`Padi_MK2_GaduTidakIjin_dblTanam`, 
 Padi_Total,
TebuRakyat_dblCemplong,
TebuRakyat_dblBibit,
TebuRakyat_dblMuda,
TebuRakyat_dblTua,
TebuRakyat_dblTotal,
TebuPerusahaan_dblCemplong,
TebuPerusahaan_dblBibit,
TebuPerusahaan_dblMuda,
TebuPerusahaan_dblTua,
TebuPerusahaan_dblTotal,
Polowijo_MH,
Polowijo_MK1_Jagung,
Polowijo_MK1_Kedelai,
Polowijo_MK1_Lainnya,
Polowijo_MK2_Jagung,
Polowijo_MK2_Kedelai,
Polowijo_MK2_Lainnya,
Polowijo_Tembakau,
Polowijo_Rosela,
Polowijo_Total,
`dblTotalLuasTanaman`,
`Bero_dblAsli`,
`Bero_dblPascaPanen`,
Bero_Total,
`dtSurvey`,
intIDuser
)
VALUES (
in_intIDJaringanIrigasi,
in_intIDSaluran,
in_dblBakuSawah,
in_txtKeterangan,
in_Debit_Intake,
in_IjinAir,
in_Debit_Efisiensi,
in_TotalTersier,
in_dblLPR,
in_dblFPR,
in_Padi_MH_dblBibit,
in_Padi_MH_dblGarap,
in_Padi_MH_dblTanam,
in_Padi_MK1_GaduIjin_dblBibit,
in_Padi_MK1_GaduIjin_dblGarap,
in_Padi_MK1_GaduIjin_dblTanam,
in_Padi_MK1_GaduTidakIjin_dblBibit,
in_Padi_MK1_GaduTidakIjin_dblGarap,
in_Padi_MK1_GaduTidakIjin_dblTanam,
in_Padi_MK2_GaduIjin_dblBibit,
in_Padi_MK2_GaduIjin_dblGarap,
in_Padi_MK2_GaduIjin_dblTanam,
in_Padi_MK2_GaduTidakIjin_dblBibit,
in_Padi_MK2_GaduTidakIjin_dblGarap,
in_Padi_MK2_GaduTidakIjin_dblTanam,
in_Padi_Total,
in_TebuRakyat_dblCemplong,
in_TebuRakyat_dblBibit,
in_TebuRakyat_dblMuda,
in_TebuRakyat_dblTua,
in_TebuRakyat_dblTotal,
in_TebuPerusahaan_dblCemplong,
in_TebuPerusahaan_dblBibit,
in_TebuPerusahaan_dblMuda,
in_TebuPerusahaan_dblTua,
in_TebuPerusahaan_dblTotal,
in_Polowijo_MH,
in_Polowijo_MK1_Jagung,
in_Polowijo_MK1_Kedelai,
in_Polowijo_MK1_Lainnya,
in_Polowijo_MK2_Jagung,
in_Polowijo_MK2_Kedelai,
in_Polowijo_MK2_Lainnya,
in_Polowijo_Tembakau,
in_Polowijo_Rosela,
in_Polowijo_Total,
in_TotalTersier + in_Padi_Total + in_TebuRakyat_dblTotal + in_TebuPerusahaan_dblTotal + in_Polowijo_Total + in_Bero_Total,
in_Bero_dblAsli,
in_Bero_dblPascaPanen,
in_Bero_Total,
in_dtSurvey,
in_intIDUser
);
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistoryJenisTanamanRusak` (IN `in_intIDSurveyKerusakanTanaman` INT, IN `in_intIDJenisTanaman` INT, IN `in_isChecked` INT)  BEGIN
IF in_isChecked = 0 THEN
	IF EXISTS (SELECT intIDSurveyKerusakanTanaman FROM tr_tanamanrusak WHERE intIDSurveyKerusakanTanaman = 	 in_intIDSurveyKerusakanTanaman and intIDJenisTanaman = in_intIDJenisTanaman) THEN
      Delete from tr_tanamanrusak where intIDSurveyKerusakanTanaman = in_intIDSurveyKerusakanTanaman and intIDJenisTanaman = in_intIDJenisTanaman;
		  select 1 as bitSuccess;
    ELSE
        select 1 as bitSuccess;
  END if;
ELSE
	IF NOT EXISTS (SELECT intIDSurveyKerusakanTanaman FROM tr_tanamanrusak WHERE intIDSurveyKerusakanTanaman = 	 in_intIDSurveyKerusakanTanaman and intIDJenisTanaman = in_intIDJenisTanaman) THEN
      INSERT INTO `tr_tanamanrusak`(
`intIDSurveyKerusakanTanaman`,
`intIDJenisTanaman`)
VALUES
( in_intIDSurveyKerusakanTanaman, in_intIDJenisTanaman);
		  select 1 as bitSuccess;
    ELSE
        select 1 as bitSuccess;
  END if;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistoryKerusakanTanaman` (IN `in_intIDSurveyKerusakanTanaman` INT, IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_intIDKecamatan` INT, IN `in_intIDDesa` BIGINT, IN `in_intIDProgramIntensifikasi` INT, IN `in_intIDPenyebabKerusakan` INT, IN `in_dblMati` DOUBLE, IN `in_dblPuso` DOUBLE, IN `in_dblBerat` DOUBLE, IN `in_dblSedang` DOUBLE, IN `in_dblRingan` DOUBLE, IN `in_dblLamaGenangan` DOUBLE, IN `in_txtKeterangan` VARCHAR(100), IN `in_dtSurvey` DATETIME)  BEGIN
UPDATE tr_surveykerusakantanaman SET 
`intIDJaringanIrigasi` = in_intIDJaringanIrigasi,
`intIDSaluran` = in_intIDSaluran,
 dblBakuSawah = in_dblBakuSawah,
intIDKecamatan = in_intIDKecamatan,
intIDDesa = in_intIDDesa,
intIDProgramIntensifikasi = in_intIDProgramIntensifikasi,
intIDPenyebabKerusakan = in_intIDPenyebabKerusakan,
dblMati = in_dblMati,
dblPuso = in_dblPuso,
dblBerat = in_dblBerat,
dblSedang = in_dblSedang,
dblRingan = in_dblRingan,
dblLamaGenangan = in_dblLamaGenangan,
txtKeterangan = in_txtKeterangan WHERE `intIDSurveyKerusakanTanaman` = in_intIDSurveyKerusakanTanaman;
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistorySurveyBangunan` (IN `in_intIDSurveyBangunan` INT, IN `in_intIDJaringanIrigasi` INT, IN `in_intIDBangunan` INT, IN `in_intIDJenisSaluran` INT, IN `in_intKondisi` INT, IN `in_intFungsi` INT, IN `in_intMempengaruhiAliranAir` INT, IN `in_txtFoto1` TEXT, IN `in_txtFoto2` TEXT, IN `in_txtFoto3` TEXT, IN `in_txtFoto4` TEXT, IN `in_txtFoto5` TEXT, IN `in_txtKeterangan` VARCHAR(250))  BEGIN

UPDATE `tr_survey_bangunan` SET `intIDJaringanIrigasi` = in_intIDJaringanIrigasi,
`intIDBangunan` = in_intIDBangunan,
`intIDJenisSaluran` = in_intIDJenisSaluran,
`intKondisi` = in_intKondisi,
`intFungsi` = in_intFungsi,
`intMempengaruhiAliranAir` = in_intMempengaruhiAliranAir,
`txtFoto1` = in_txtFoto1,
`txtFoto2` = in_txtFoto2,
`txtFoto3` = in_txtFoto3,
`txtFoto4` = in_txtFoto4,
`txtFoto5` = in_txtFoto5,
`txtKeterangan` = in_txtKeterangan
WHERE `intIDSurveyBangunan` = in_intIDSurveyBangunan;
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistorySurveyCurahHujan` (IN `in_intIDCurahHujan` INT, IN `in_dblCurahHujan` DOUBLE)  BEGIN
  update tr_surveycurahhujan set dblCurahHujan = in_dblCurahHujan
where intIDCurahHujan = in_intIDCurahHujan;
select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistorySurveyPintuAir` (IN `in_intIDSurveyPintuAir` INT, IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_Pelaksanaan_dblDebit` DOUBLE, IN `in_Pelaksanaan_dblLPR` DOUBLE, IN `in_Pelaksanaan_dblFPR` DOUBLE, IN `in_Perencanaan_dblDebit` DOUBLE, IN `in_Perencanaan_dblLPR` DOUBLE, IN `in_Perencanaan_dblFPR` DOUBLE, IN `in_dtSurvey` DATETIME)  BEGIN
UPDATE tr_surveypintuair SET 
`intIDJaringanIrigasi` = in_intIDJaringanIrigasi,
`intIDSaluran` = in_intIDSaluran,
 dblBakuSawah = in_dblBakuSawah,
`Pelaksanaan_dblDebit` = in_Pelaksanaan_dblDebit, 
`Pelaksanaan_dblLPR` = in_Pelaksanaan_dblLPR,
`Pelaksanaan_dblFPR` = in_Pelaksanaan_dblFPR,
`Perencanaan_dblDebit` = in_Perencanaan_dblDebit,
`Perencanaan_dblLPR` = in_Perencanaan_dblLPR,
`Perencanaan_dblFPR` = in_Perencanaan_dblFPR WHERE `intIDSurveyPintuAir` = in_intIDSurveyPintuAir;
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_updateHistorySurveyRealisasiTanaman` (IN `in_intIDSurveyRealisasiTanaman` INT, IN `in_intIDJaringanIrigasi` INT, IN `in_intIDSaluran` INT, IN `in_dblBakuSawah` DOUBLE, IN `in_txtKeterangan` VARCHAR(150), IN `in_Debit_Intake` DOUBLE, IN `in_IjinAir` DOUBLE, IN `in_Debit_Efisiensi` DOUBLE, IN `in_TotalTersier` DOUBLE, IN `in_dblLPR` DOUBLE, IN `in_dblFPR` DOUBLE, IN `in_Padi_MH_dblBibit` DOUBLE, IN `in_Padi_MH_dblGarap` DOUBLE, IN `in_Padi_MH_dblTanam` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblBibit` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblGarap` DOUBLE, IN `in_Padi_MK1_GaduIjin_dblTanam` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblBibit` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblGarap` DOUBLE, IN `in_Padi_MK1_GaduTidakIjin_dblTanam` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblBibit` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblGarap` DOUBLE, IN `in_Padi_MK2_GaduIjin_dblTanam` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblBibit` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblGarap` DOUBLE, IN `in_Padi_MK2_GaduTidakIjin_dblTanam` DOUBLE, IN `in_Padi_Total` DOUBLE, IN `in_TebuRakyat_dblCemplong` DOUBLE, IN `in_TebuRakyat_dblBibit` DOUBLE, IN `in_TebuRakyat_dblMuda` DOUBLE, IN `in_TebuRakyat_dblTua` DOUBLE, IN `in_TebuRakyat_dblTotal` DOUBLE, IN `in_TebuPerusahaan_dblCemplong` DOUBLE, IN `in_TebuPerusahaan_dblBibit` DOUBLE, IN `in_TebuPerusahaan_dblMuda` DOUBLE, IN `in_TebuPerusahaan_dblTua` DOUBLE, IN `in_TebuPerusahaan_dblTotal` DOUBLE, IN `in_Polowijo_MH` DOUBLE, IN `in_Polowijo_MK1_Jagung` DOUBLE, IN `in_Polowijo_MK1_Kedelai` DOUBLE, IN `in_Polowijo_MK1_Lainnya` DOUBLE, IN `in_Polowijo_MK2_Jagung` DOUBLE, IN `in_Polowijo_MK2_Kedelai` DOUBLE, IN `in_Polowijo_MK2_Lainnya` DOUBLE, IN `in_Polowijo_Tembakau` DOUBLE, IN `in_Polowijo_Rosela` DOUBLE, IN `in_Polowijo_Total` DOUBLE, IN `in_Bero_dblAsli` DOUBLE, IN `in_Bero_dblPascaPanen` DOUBLE, IN `in_Bero_Total` DOUBLE, IN `in_dtSurvey` DATETIME, IN `in_intIDUser` INT)  BEGIN


UPDATE tr_surveyrealisasitanaman SET 
`intIDJaringanIrigasi` = in_intIDJaringanIrigasi,
`intIDSaluran` = in_intIDSaluran,
`dblBakuSawah` = in_dblBakuSawah,
`txtKeterangan` = in_txtKeterangan,
`Padi_MH_dblBibit` = in_Padi_MH_dblBibit,
`Padi_MH_dblGarap` = in_Padi_MH_dblGarap, 
`Padi_MH_dblTanam` = in_Padi_MH_dblTanam,
`Padi_MK1_GaduIjin_dblBibit` = in_Padi_MK1_GaduIjin_dblBibit,
`Padi_MK1_GaduIjin_dblGarap` = in_Padi_MK1_GaduIjin_dblGarap,
`Padi_MK1_GaduIjin_dblTanam` = in_Padi_MK1_GaduIjin_dblTanam, 
`Padi_MK1_GaduTidakIjin_dblBibit` = in_Padi_MK1_GaduTidakIjin_dblBibit,
`Padi_MK1_GaduTidakIjin_dblGarap` = in_Padi_MK1_GaduTidakIjin_dblGarap,
`Padi_MK1_GaduTidakIjin_dblTanam` = in_Padi_MK1_GaduTidakIjin_dblTanam, 
`Padi_MK2_GaduIjin_dblBibit` = in_Padi_MK2_GaduIjin_dblBibit,
`Padi_MK2_GaduIjin_dblGarap` = in_Padi_MK2_GaduIjin_dblGarap,
`Padi_MK2_GaduIjin_dblTanam` = in_Padi_MK2_GaduIjin_dblTanam,
`Padi_MK2_GaduTidakIjin_dblBibit` = in_Padi_MK2_GaduTidakIjin_dblBibit,
`Padi_MK2_GaduTidakIjin_dblGarap` = in_Padi_MK2_GaduTidakIjin_dblGarap,
`Padi_MK2_GaduTidakIjin_dblTanam` = in_Padi_MK2_GaduTidakIjin_dblTanam,
`Padi_Total` = in_Padi_Total, 
`TebuRakyat_dblCemplong` = in_TebuRakyat_dblCemplong, 
`TebuRakyat_dblBibit` = in_TebuRakyat_dblBibit, 
`TebuRakyat_dblMuda` = in_TebuRakyat_dblMuda,
`TebuRakyat_dblTua` = in_TebuRakyat_dblTua,
`TebuRakyat_dblTotal` = in_TebuRakyat_dblTotal,
`TebuPerusahaan_dblCemplong` = in_TebuPerusahaan_dblCemplong,
`TebuPerusahaan_dblBibit` = in_TebuPerusahaan_dblBibit,
`TebuPerusahaan_dblMuda` = in_TebuPerusahaan_dblMuda,
`TebuPerusahaan_dblTua` = in_TebuPerusahaan_dblTua,
`TebuPerusahaan_dblTotal` = in_TebuPerusahaan_dblTotal, 
`Polowijo_MH` = in_Polowijo_MH,
`Polowijo_MK1_Jagung` = in_Polowijo_MK1_Jagung, 
`Polowijo_MK1_Kedelai` = in_Polowijo_MK1_Kedelai,
`Polowijo_MK1_Lainnya` = in_Polowijo_MK1_Lainnya,
`Polowijo_MK2_Jagung` = in_Polowijo_MK2_Jagung,
`Polowijo_MK2_Kedelai` = in_Polowijo_MK2_Kedelai,
`Polowijo_MK2_Lainnya` = in_Polowijo_MK2_Lainnya,
`Polowijo_Tembakau` = in_Polowijo_Tembakau,
`Polowijo_Rosela` = in_Polowijo_Rosela,
`Polowijo_Total` = in_Polowijo_Total, 
`Bero_dblAsli` = in_Bero_dblAsli,
`Bero_dblPascaPanen` = in_Bero_dblPascaPanen,
`Bero_Total` = in_Bero_Total,
`intIDUser` = in_intIDUser,
`Debit_Intake` = in_Debit_Intake,
`Debit_IjinAir` = in_Debit_IjinAir, 
`Debit_TotalTersier` = in_Debit_TotalTersier,
`Debit_Efisiensi` = in_Debit_Efisiensi,
`dblLPR` = in_dblLPR,
`dblFPR` = in_dblFPR, 
`dblTotalLuasTanaman` = in_dblTotalLuasTanaman, 
`dtUpdate` = in_in_dtSurvey
WHERE 
`intIDSurveyRealisasiTanaman` = in_intIDSurveyRealisasiTanaman;
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_userCheckPass` (IN `in_intIDuser` INT, IN `in_txtPassword` VARCHAR(50))  BEGIN
 if EXISTS(select intIDuser from m_user where intIDuser = in_intIDuser and txtPassword = in_txtPassword and isActive = 1)
   then
     select 1 as bitSuccess;
   else
       select 0 as bitSuccess;
   end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_userLogin` (IN `in_txtUsername` VARCHAR(50), IN `in_txtPassword` VARCHAR(50))  BEGIN
 if EXISTS(select intIDuser from m_user where (txtUsername = in_txtUsername OR txtEmail = in_txtUsername) and txtPassword = in_txtPassword and isActive = 1)
   then
     select 1 as bitSuccess,intIDuser, txtUsername,txtEmail,txtFirstName from m_user where (txtUsername = in_txtUsername OR txtEmail = in_txtUsername) and txtPassword = in_txtPassword and isActive = 1;
   else
       select 0 as bitSuccess, 0 as intIDuser, '' as txtUsername, '' as txtEmail, '' as txtFirstName ;
   end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mobile_userUpdatePassword` (IN `in_intIDuser` INT, IN `in_txtPassword` VARCHAR(50))  BEGIN
UPDATE m_user SET txtPassword = in_txtPassword where intIDuser = in_intIDuser;
 
 select 1 as bitSuccess;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rpt_realisasitanaman` (IN `in_dtstart` DATE, IN `in_end` DATE)  BEGIN

select TR.dtSurvey,
(SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(SELECT J.txtSaluran from m_saluran J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi AND J.intIDSaluran = TR.intIDSaluran) as txtSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = (select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran limit 1) ) as txtJenisSaluran,
TR.dblBakuSawah, 
TR.Padi_MH_dblBibit,TR.Padi_MH_dblGarap,TR.Padi_MH_dblTanam,TR.Padi_MK1_GaduIjin_dblBibit,TR.Padi_MK1_GaduIjin_dblGarap, TR.Padi_MK1_GaduIjin_dblTanam,TR.Padi_MK1_GaduTidakIjin_dblBibit,
TR.Padi_MK1_GaduTidakIjin_dblGarap,TR.Padi_MK1_GaduTidakIjin_dblTanam,TR.Padi_MK2_GaduIjin_dblBibit,
TR.Padi_MK2_GaduIjin_dblBibit,TR.Padi_MK2_GaduIjin_dblGarap,TR.Padi_MK2_GaduIjin_dblTanam,TR.Padi_MK2_GaduTidakIjin_dblBibit, TR.Padi_MK2_GaduTidakIjin_dblGarap, TR.Padi_MK2_GaduTidakIjin_dblTanam,
TR.Tebu_dblTua, TR.Tebu_dblMuda, TR.Tebu_dblBibit, TR.Tebu_dblGarap, TR.Polowijo_MH, TR.Polowijo_MK1,TR.Polowijo_MK2, TR.dblTotalLuasTanaman, TR.Bero_dblAsli, TR.Bero_dblPascaPanen, TR.dblLPR, TR.dblFPR

from tr_surveyrealisasitanaman TR
where TR.dtSurvey >= in_dtstart and TR.dtSurvey <= in_end order by TR.dtSurvey ASC;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rpt_surveybangunan` (IN `in_dtstart` DATE, IN `in_end` DATE)  BEGIN
select TR.dtSurvey,
(SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(SELECT J.txtBangunan from m_bangunan_irigasi J where J.intIDBangunan = TR.intIDBangunan) as txtBangunan,
(SELECT J.txtSaluran from m_saluran J where J.intIDJenisSaluran = TR.intIDJenisSaluran) as txtSaluran,
TR.dblLatitude, TR.dblLongitude, TR.intKondisi, TR.intFungsi,  TR.intMempengaruhiAliranAir,
TR.txtFoto1,TR.txtFoto2,TR.txtFoto3,TR.txtFoto4,TR.txtFoto5,TR.txtKeterangan
from tr_survey_bangunan TR

where TR.dtSurvey >= in_dtstart and TR.dtSurvey <= in_end order by TR.dtSurvey ASC;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rpt_surveycurahhujan` (IN `in_dtstart` DATE, IN `in_end` DATE)  BEGIN

select TR.dtSurvey,
(SELECT K.txtKecamatan from m_kecamatan K where K.intIDKecamatan = TR.intIDKecamatan) as txtKecamatan,
(SELECT C.txtStasiunCurahHujan from m_stasiuncurahhujan C where C.intIDStasiunCurahHujan = TR.intIDStasiunCurahHujan) as txtStasiunCurahHujan,
(SELECT C.txtNo from m_stasiuncurahhujan C where C.intIDStasiunCurahHujan = TR.intIDStasiunCurahHujan) as txtNo,
(SELECT C.txtElevasi from m_stasiuncurahhujan C where C.intIDStasiunCurahHujan = TR.intIDStasiunCurahHujan) as txtElevasi,
TR.dblCurahHujan
from tr_surveycurahhujan TR where TR.dtSurvey >= in_dtstart and TR.dtSurvey <= in_end order by TR.dtSurvey ASC;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rpt_surveykerusakantanaman` (IN `in_dtstart` DATE, IN `in_end` DATE)  BEGIN

select TR.intIDSurveyKerusakanTanaman, TR.dtSurvey,
'Lumajang',
(select K.txtKecamatan from m_kecamatan K where K.intIDKecamatan = TR.intIDKecamatan) txtKecamatan,
(select K.txtDesa from m_desa K where K.intIDKecamatan = TR.intIDKecamatan and K.intIDDesa = TR.intIDDesa) txtDesa,
(SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(SELECT J.txtSaluran from m_saluran J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi AND J.intIDSaluran = TR.intIDSaluran) as txtSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = (select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran limit 1) ) as txtJenisSaluran,
TM.intIDJenisTanaman,
(select J.txtJenisTanaman from m_jenistanaman J where J.intIDJenisTanaman = TM.intIDJenisTanaman) txtJenisTanaman,
TR.dblBakuSawah,
(select P.txtProgramIntensifikasi from m_programintensifikasi P where P.intIDProgramIntensifikasi = TR.intIDProgramIntensifikasi) txtProgramIntensifikasi,
(select P.txtPenyebabKerusakan from m_penyebabkerusakan P where P.intIDPenyebabKerusakan = TR.intIDPenyebabKerusakan) txtPenyebabKerusakan, TR.dblMati, TR.dblPuso, TR.dblBerat, TR.dblRingan, TR.dblLamaGenangan, TR.txtKeterangan

FROM tr_surveykerusakantanaman TR INNER JOIN tr_tanamanrusak TM ON TR.intIDSurveyKerusakanTanaman = TM.intIDSurveyKerusakanTanaman

where TR.dtSurvey >= in_dtstart and TR.dtSurvey <= in_end order by TR.dtSurvey ASC;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rpt_surveypintuair` (IN `in_dtstart` DATE, IN `in_end` DATE)  BEGIN

select TR.dtSurvey,
(SELECT J.txtJaringanIrigasi from m_jaringanirigasi J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi) as txtJaringanIrigasi,
(SELECT J.txtSaluran from m_saluran J where J.intIDJaringanIrigasi = TR.intIDJaringanIrigasi AND J.intIDSaluran = TR.intIDSaluran) as txtSaluran,
(select JS.txtJenisSaluran from m_jenissaluran JS where JS.intIDJenisSaluran = (select S.intIDJenisSaluran from m_saluran S where S.intIDSaluran = TR.intIDSaluran limit 1) ) as txtJenisSaluran,
TR.dblBakuSawah, 
TR.Pelaksanaan_dblDebit, TR.Pelaksanaan_dblLPR,TR.Pelaksanaan_dblFPR,
TR.Perencanaan_dblDebit, TR.Perencanaan_dblLPR, TR.Perencanaan_dblFPR

from tr_surveypintuair TR
where TR.dtSurvey >= in_dtstart and TR.dtSurvey <= in_end order by TR.dtSurvey ASC;


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_bangunan_irigasi`
--

CREATE TABLE `m_bangunan_irigasi` (
  `intIDBangunan` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `txtBangunan` varchar(50) DEFAULT NULL,
  `intIDJenisSaluranBangunan` int(11) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_bangunan_irigasi`
--

INSERT INTO `m_bangunan_irigasi` (`intIDBangunan`, `intIDJaringanIrigasi`, `txtBangunan`, `intIDJenisSaluranBangunan`, `dblLatitude`, `dblLongitude`) VALUES
(1, 1, 'Ranu Pakis', 1, -7.78909450, 110.36354860),
(2, 1, 'Ranu pali', 2, -7.96043430, 112.71955440),
(3, 1, 'DPUTR', 2, -8.13411930, NULL),
(4, 17, 'Bangunan Contoh Edit', 2, -8.13411930, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_desa`
--

CREATE TABLE `m_desa` (
  `intIDDesa` bigint(11) NOT NULL,
  `intIDKecamatan` int(11) DEFAULT NULL,
  `txtDesa` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_desa`
--

INSERT INTO `m_desa` (`intIDDesa`, `intIDKecamatan`, `txtDesa`) VALUES
(3508010001, 3508010, 'TEGALREJO'),
(3508010002, 3508010, 'BULUREJO'),
(3508010003, 3508010, 'PUROREJO'),
(3508010004, 3508010, 'TEMPURREJO'),
(3508010005, 3508010, 'TEMPURSARI'),
(3508010006, 3508010, 'PUNDUNGSARI'),
(3508010007, 3508010, 'KALIULING'),
(3508020001, 3508020, 'SIDOMULYO'),
(3508020002, 3508020, 'PRONOJIWO'),
(3508020003, 3508020, 'TAMANAYU'),
(3508020004, 3508020, 'SUMBERURIP'),
(3508020005, 3508020, 'ORO ORO OMBO'),
(3508020006, 3508020, 'SUPITURANG'),
(3508030001, 3508030, 'JUGOSARI'),
(3508030002, 3508030, 'JARIT'),
(3508030003, 3508030, 'CANDIPURO'),
(3508030004, 3508030, 'SUMBERREJO'),
(3508030005, 3508030, 'SUMBERWULUH'),
(3508030006, 3508030, 'SUMBERMUJUR'),
(3508030007, 3508030, 'PENANGGAL'),
(3508030008, 3508030, 'TAMBAHREJO'),
(3508030009, 3508030, 'KLOPOSAWIT'),
(3508030010, 3508030, 'TUMPENG'),
(3508040001, 3508040, 'GONDORUSO'),
(3508040002, 3508040, 'KALIBENDO'),
(3508040003, 3508040, 'BADES'),
(3508040004, 3508040, 'BAGO'),
(3508040005, 3508040, 'SELOK AWAR AWAR'),
(3508040006, 3508040, 'CONDRO'),
(3508040007, 3508040, 'MADUREJO'),
(3508040008, 3508040, 'PASIRIAN'),
(3508040009, 3508040, 'SEMEMU'),
(3508040010, 3508040, 'NGUTER'),
(3508040011, 3508040, 'SELOK ANYAR'),
(3508050001, 3508050, 'PANDANARUM'),
(3508050002, 3508050, 'PANDANWANGI'),
(3508050003, 3508050, 'SUMBERJATI'),
(3508050004, 3508050, 'TEMPEH KIDUL'),
(3508050005, 3508050, 'LEMPENI'),
(3508050006, 3508050, 'TEMPEH TENGAH'),
(3508050007, 3508050, 'KALIWUNGU'),
(3508050008, 3508050, 'TEMPEH LOR'),
(3508050009, 3508050, 'BESUK'),
(3508050010, 3508050, 'JATISARI'),
(3508050011, 3508050, 'PULO'),
(3508050012, 3508050, 'GESANG'),
(3508050013, 3508050, 'JOKARTO'),
(3508060008, 3508060, 'BANJARWARU'),
(3508060009, 3508060, 'LABRUK LOR'),
(3508060010, 3508060, 'CITRODIWANGSAN'),
(3508060011, 3508060, 'DITOTRUNAN'),
(3508060012, 3508060, 'JOGOTRUNAN'),
(3508060013, 3508060, 'DENOK'),
(3508060014, 3508060, 'BLUKON'),
(3508060015, 3508060, 'BORENG'),
(3508060016, 3508060, 'JOGOYUDAN'),
(3508060017, 3508060, 'ROGOTRUNAN'),
(3508060018, 3508060, 'TOMPOKERSAN'),
(3508060019, 3508060, 'KEPUHARJO'),
(3508061001, 3508061, 'SUMBERSUKO'),
(3508061002, 3508061, 'KEBONSARI'),
(3508061003, 3508061, 'GRATI'),
(3508061004, 3508061, 'LABRUK KIDUL'),
(3508061005, 3508061, 'MOJOSARI'),
(3508061006, 3508061, 'SENTUL'),
(3508061007, 3508061, 'PURWOSONO'),
(3508061008, 3508061, 'PETAHUNAN'),
(3508070001, 3508070, 'WONOGRIYO'),
(3508070002, 3508070, 'WONOSARI'),
(3508070003, 3508070, 'MANGUNSARI'),
(3508070004, 3508070, 'TEKUNG'),
(3508070005, 3508070, 'WONOKERTO'),
(3508070006, 3508070, 'TUKUM'),
(3508070007, 3508070, 'KARANGBENDO'),
(3508070008, 3508070, 'KLAMPOKARUM'),
(3508080001, 3508080, 'JATIMULYO'),
(3508080002, 3508080, 'JATIREJO'),
(3508080003, 3508080, 'JATIGONO'),
(3508080004, 3508080, 'SUKOREJO'),
(3508080005, 3508080, 'SUKOSARI'),
(3508080006, 3508080, 'KUNIR KIDUL'),
(3508080007, 3508080, 'KUNIR LOR'),
(3508080008, 3508080, 'KEDUNGMORO'),
(3508080009, 3508080, 'KARANGLO'),
(3508080010, 3508080, 'KABUARAN'),
(3508080011, 3508080, 'DOROGOWOK'),
(3508090001, 3508090, 'DARUNGAN'),
(3508090002, 3508090, 'KRATON'),
(3508090003, 3508090, 'WOTGALIH'),
(3508090004, 3508090, 'TUNJUNGREJO'),
(3508090005, 3508090, 'YOSOWILANGUN KIDUL'),
(3508090006, 3508090, 'YOSOWILANGUN LOR'),
(3508090007, 3508090, 'KRAI'),
(3508090008, 3508090, 'KARANGAYAR'),
(3508090009, 3508090, 'KARANGREJO'),
(3508090010, 3508090, 'MUNDER'),
(3508090011, 3508090, 'KEBONSARI'),
(3508090012, 3508090, 'KALIPEPE'),
(3508100001, 3508100, 'NOGOSARI'),
(3508100002, 3508100, 'KEDUNGREJO'),
(3508100003, 3508100, 'SIDOREJO'),
(3508100004, 3508100, 'ROWOKANGKUNG'),
(3508100005, 3508100, 'SUMBERSARI'),
(3508100006, 3508100, 'SUMBERANYAR'),
(3508100007, 3508100, 'DAWUHAN WETAN'),
(3508110001, 3508110, 'BANYUPUTIH KIDUL'),
(3508110002, 3508110, 'ROJOPOLO'),
(3508110003, 3508110, 'SUKOSARI'),
(3508110004, 3508110, 'KALIBOTO KIDUL'),
(3508110005, 3508110, 'KALIBOTO LOR'),
(3508110006, 3508110, 'JATIROTO'),
(3508120001, 3508120, 'BANYUPUTIH LOR'),
(3508120002, 3508120, 'KALIDILEM'),
(3508120003, 3508120, 'TUNJUNG'),
(3508120004, 3508120, 'GEDANGMAS'),
(3508120005, 3508120, 'KALIPENGGUNG'),
(3508120006, 3508120, 'RANULOGONG'),
(3508120007, 3508120, 'RANDUAGUNG'),
(3508120008, 3508120, 'LEDOKTEMPURO'),
(3508120009, 3508120, 'PEJARAKAN'),
(3508120010, 3508120, 'BUWEK'),
(3508120011, 3508120, 'RANUWURUNG'),
(3508120012, 3508120, 'SALAK'),
(3508130001, 3508130, 'KLANTING'),
(3508130002, 3508130, 'KEBONAGUNG'),
(3508130003, 3508130, 'KARANGSARI'),
(3508130004, 3508130, 'DAWUHAN LOR'),
(3508130005, 3508130, 'KUTORENON'),
(3508130006, 3508130, 'SELOKBESUKI'),
(3508130007, 3508130, 'SUMBEREJO'),
(3508130008, 3508130, 'URANGGANTUNG'),
(3508130009, 3508130, 'SELOKGONDANG'),
(3508130010, 3508130, 'BONDOYUDO'),
(3508140001, 3508140, 'BARAT'),
(3508140002, 3508140, 'BABAKAN'),
(3508140003, 3508140, 'MOJO'),
(3508140004, 3508140, 'BODANG'),
(3508140005, 3508140, 'KEDAWUNG'),
(3508140006, 3508140, 'PADANG'),
(3508140007, 3508140, 'KALISEMUT'),
(3508140008, 3508140, 'MERAKAN'),
(3508140009, 3508140, 'TANGGUNG'),
(3508150001, 3508150, 'PASRUJAMBE'),
(3508150002, 3508150, 'JAMBEKUMBU'),
(3508150003, 3508150, 'SUKOREJO'),
(3508150004, 3508150, 'JAMBEARUM'),
(3508150005, 3508150, 'KERTOSARI'),
(3508150006, 3508150, 'PAGOWAN'),
(3508150007, 3508150, 'KARANGANOM'),
(3508160002, 3508160, 'PURWOREJO'),
(3508160003, 3508160, 'SARIKEMUNING'),
(3508160004, 3508160, 'PANDANSARI'),
(3508160005, 3508160, 'SENDURO'),
(3508160006, 3508160, 'BURNO'),
(3508160007, 3508160, 'KANDANGTEPUS'),
(3508160008, 3508160, 'KANDANGAN'),
(3508160009, 3508160, 'BEDAYU'),
(3508160010, 3508160, 'BEDAYUTALANG'),
(3508160011, 3508160, 'WONOCEPOKOAYU'),
(3508160012, 3508160, 'ARGOSARI'),
(3508160013, 3508160, 'RANUPANE'),
(3508170001, 3508170, 'WONOKERTO'),
(3508170002, 3508170, 'PAKEL'),
(3508170003, 3508170, 'KENONGO'),
(3508170004, 3508170, 'GUCIALIT'),
(3508170005, 3508170, 'DADAPAN'),
(3508170006, 3508170, 'KERTOWONO'),
(3508170007, 3508170, 'TUNJUNG'),
(3508170008, 3508170, 'JERUK'),
(3508170009, 3508170, 'SOMBO'),
(3508180001, 3508180, 'PANDANSARI'),
(3508180002, 3508180, 'KRASAK'),
(3508180003, 3508180, 'KEDUNGJAJANG'),
(3508180004, 3508180, 'WONOREJO'),
(3508180005, 3508180, 'UMBUL'),
(3508180006, 3508180, 'CURAHPETUNG'),
(3508180007, 3508180, 'GROBOGAN'),
(3508180008, 3508180, 'BENCE'),
(3508180009, 3508180, 'JATISARI'),
(3508180010, 3508180, 'TEMPURSARI'),
(3508180011, 3508180, 'BANDARAN'),
(3508180012, 3508180, 'SAWARAN KULON'),
(3508190001, 3508190, 'KEBONAN'),
(3508190002, 3508190, 'KUDUS'),
(3508190003, 3508190, 'DUREN'),
(3508190004, 3508190, 'SUMBERWRINGIN'),
(3508190005, 3508190, 'PAPRINGAN'),
(3508190006, 3508190, 'RANUPAKIS'),
(3508190007, 3508190, 'TEGALRANDU'),
(3508190008, 3508190, 'KLAKAH'),
(3508190009, 3508190, 'MLAWANG'),
(3508190010, 3508190, 'SRUNI'),
(3508190011, 3508190, 'TEGALCIUT'),
(3508190012, 3508190, 'SAWARAN LOR'),
(3508200001, 3508200, 'JENGGRONG'),
(3508200002, 3508200, 'MENINJO'),
(3508200003, 3508200, 'TEGALBANGSRI'),
(3508200004, 3508200, 'SUMBERPETUNG'),
(3508200005, 3508200, 'ALUN ALUN'),
(3508200006, 3508200, 'RANUBEDALI'),
(3508200007, 3508200, 'RANUYOSO'),
(3508200008, 3508200, 'WONOAYU'),
(3508200009, 3508200, 'PENAWUNGAN'),
(3508200010, 3508200, 'WATES KULON'),
(3508200011, 3508200, 'WATES WETAN');

-- --------------------------------------------------------

--
-- Table structure for table `m_jaringanirigasi`
--

CREATE TABLE `m_jaringanirigasi` (
  `intIDJaringanIrigasi` int(11) NOT NULL,
  `txtKodeSingkatan` varchar(50) DEFAULT NULL,
  `txtJaringanIrigasi` varchar(50) DEFAULT NULL,
  `intIDUPT` int(11) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL,
  `intIDKewenanganJaringan` int(11) DEFAULT NULL,
  `dblBakuSawah` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_jaringanirigasi`
--

INSERT INTO `m_jaringanirigasi` (`intIDJaringanIrigasi`, `txtKodeSingkatan`, `txtJaringanIrigasi`, `intIDUPT`, `dblLatitude`, `dblLongitude`, `intIDKewenanganJaringan`, `dblBakuSawah`) VALUES
(1, '335080220', 'Ali', NULL, NULL, NULL, NULL, NULL),
(2, '335080052', 'Bacem I', NULL, NULL, NULL, NULL, NULL),
(3, '-', 'Bacem II', NULL, NULL, NULL, NULL, NULL),
(4, '-', 'Banjarejo', NULL, NULL, NULL, NULL, NULL),
(5, '335080174', 'Bayur', NULL, NULL, NULL, NULL, NULL),
(6, '335080064', 'Bedayu', NULL, NULL, NULL, NULL, NULL),
(7, '335080061', 'Bendo', NULL, NULL, NULL, NULL, NULL),
(8, '335080050', 'Bentrong Suko', NULL, NULL, NULL, NULL, NULL),
(9, '335080130', 'Besuki', NULL, NULL, NULL, NULL, NULL),
(10, '335080221', 'Betoto I', NULL, NULL, NULL, NULL, NULL),
(11, '335080031', 'Betoto II', NULL, NULL, NULL, NULL, NULL),
(12, '335080043', 'Bisri', NULL, NULL, NULL, NULL, NULL),
(13, '335080122', 'Blok Rejo', NULL, NULL, NULL, NULL, NULL),
(14, '335080003', 'Blukon', NULL, NULL, NULL, NULL, NULL),
(15, '335080042', 'Bodang', NULL, NULL, NULL, NULL, NULL),
(16, '335080024', 'Boreng', NULL, NULL, NULL, NULL, NULL),
(17, '335080028', 'Boto', NULL, NULL, NULL, NULL, NULL),
(18, '335080149', 'Bulak Klakah', NULL, NULL, NULL, NULL, NULL),
(19, '335080223', 'Bulak Manggis', NULL, NULL, NULL, NULL, NULL),
(20, '335080150', 'Candri', NULL, NULL, NULL, NULL, NULL),
(21, '335080267', 'Cangkring I', NULL, NULL, NULL, NULL, NULL),
(22, '335080224', 'Cangkring II', NULL, NULL, NULL, NULL, NULL),
(23, '335080197', 'Carang Kuning', NULL, NULL, NULL, NULL, NULL),
(24, '-', 'Celeng Mati', NULL, NULL, NULL, NULL, NULL),
(25, '335080209', 'Curah Petung I', NULL, NULL, NULL, NULL, NULL),
(26, '-', 'Curah Petung II', NULL, NULL, NULL, NULL, NULL),
(27, '335080129', 'Curah Wedi I', NULL, NULL, NULL, NULL, NULL),
(28, '335080107', 'Curah Wedi II', NULL, NULL, NULL, NULL, NULL),
(29, '335080086', 'Dadapan', NULL, NULL, NULL, NULL, NULL),
(30, '335080170', 'Darungan', NULL, NULL, NULL, NULL, NULL),
(31, '335080070', 'Demo', NULL, NULL, NULL, NULL, NULL),
(32, '335080072', 'Depok', NULL, NULL, NULL, NULL, NULL),
(33, '335080013', 'Dilem', NULL, NULL, NULL, NULL, NULL),
(34, '-', 'Djatim', NULL, NULL, NULL, NULL, NULL),
(35, '-', 'Dompyong I', NULL, NULL, NULL, NULL, NULL),
(36, '-', 'Dompyong II', NULL, NULL, NULL, NULL, NULL),
(37, '335080006', 'Duk', NULL, NULL, NULL, NULL, NULL),
(38, '335080125', 'Duk I', NULL, NULL, NULL, NULL, NULL),
(39, '335080082', 'Duk II', NULL, NULL, NULL, NULL, NULL),
(40, '335080228', 'Duk III', NULL, NULL, NULL, NULL, NULL),
(41, '335080229', 'Duk IV', NULL, NULL, NULL, NULL, NULL),
(42, '335080226', 'Duk V', NULL, NULL, NULL, NULL, NULL),
(43, '335080059', 'Dul Mungit', NULL, NULL, NULL, NULL, NULL),
(44, '335080131', 'Durek', NULL, NULL, NULL, NULL, NULL),
(45, '335080187', 'Duren I', NULL, NULL, NULL, NULL, NULL),
(46, '335080117', 'Duren II', NULL, NULL, NULL, NULL, NULL),
(47, '335080233', 'Duren III', NULL, NULL, NULL, NULL, NULL),
(48, '335080231', 'Duren IV', NULL, NULL, NULL, NULL, NULL),
(49, '335080232', 'Duren V', NULL, NULL, NULL, NULL, NULL),
(50, '335080078', 'Duren VI', NULL, NULL, NULL, NULL, NULL),
(51, '335080065', 'Gedang Mas', NULL, NULL, NULL, NULL, NULL),
(52, '335080045', 'Gedang Mas I', NULL, NULL, NULL, NULL, NULL),
(53, '335080009', 'Gedang Mas II', NULL, NULL, NULL, NULL, NULL),
(54, '335080134', 'Gedangan', NULL, NULL, NULL, NULL, NULL),
(55, '335080234', 'Gempol', NULL, NULL, NULL, NULL, NULL),
(56, '335080001', 'Gubug Domas', NULL, NULL, NULL, NULL, NULL),
(57, '335080151', 'Ismail', NULL, NULL, NULL, NULL, NULL),
(58, '335080196', 'Jabon', NULL, NULL, NULL, NULL, NULL),
(59, '335080181', 'Jagalan ', NULL, NULL, NULL, NULL, NULL),
(60, '335080145', 'Jagalan I', NULL, NULL, NULL, NULL, NULL),
(61, '335080169', 'Jamal', NULL, NULL, NULL, NULL, NULL),
(62, '-', 'Jambe', NULL, NULL, NULL, NULL, NULL),
(63, '335080087', 'Jati', NULL, NULL, NULL, NULL, NULL),
(64, '335080236', 'Jemuit', NULL, NULL, NULL, NULL, NULL),
(65, '335080315', 'Jerukan/Monisa', NULL, NULL, NULL, NULL, NULL),
(66, '335080237', 'Jombok I', NULL, NULL, NULL, NULL, NULL),
(67, '335080235', 'Jombok II', NULL, NULL, NULL, NULL, NULL),
(68, '335080238', 'Jombok III', NULL, NULL, NULL, NULL, NULL),
(69, '335080051', 'Jonggrang I', NULL, NULL, NULL, NULL, NULL),
(70, '335080166', 'Jonggrang II', NULL, NULL, NULL, NULL, NULL),
(71, '335080178', 'Jonggrang III', NULL, NULL, NULL, NULL, NULL),
(72, '335080239', 'Jonggrang IV', NULL, NULL, NULL, NULL, NULL),
(73, '-', 'Kali Wuluh', NULL, NULL, NULL, NULL, NULL),
(74, '335080245', 'Kali Piri', NULL, NULL, NULL, NULL, NULL),
(75, '335080184', 'Kamid', NULL, NULL, NULL, NULL, NULL),
(76, '335080157', 'Karang Anyar I', NULL, NULL, NULL, NULL, NULL),
(77, '335080165', 'Karang Anyar II', NULL, NULL, NULL, NULL, NULL),
(78, '335080088', 'Karang Culik', NULL, NULL, NULL, NULL, NULL),
(79, '335080240', 'Karang Kletak', NULL, NULL, NULL, NULL, NULL),
(80, '335080143', 'Kebon Deli I', NULL, NULL, NULL, NULL, NULL),
(81, '335080108', 'Kebon Deli II', NULL, NULL, NULL, NULL, NULL),
(82, '335080141', 'Kebon Deli III', NULL, NULL, NULL, NULL, NULL),
(83, '335080139', 'Kebon Deli IV', NULL, NULL, NULL, NULL, NULL),
(84, '335080222', 'Kebon Deli V', NULL, NULL, NULL, NULL, NULL),
(85, '335080016', 'Kedung Caring', NULL, NULL, NULL, NULL, NULL),
(86, '335080217', 'Kedung Klampok', NULL, NULL, NULL, NULL, NULL),
(87, '335080215', 'Kedung Lier I', NULL, NULL, NULL, NULL, NULL),
(88, '335080241', 'Kedung Lier II', NULL, NULL, NULL, NULL, NULL),
(89, '-', 'Kemiri I', NULL, NULL, NULL, NULL, NULL),
(90, '-', 'Kemiri II', NULL, NULL, NULL, NULL, NULL),
(91, '335080010', 'Klakah Pakis', NULL, NULL, NULL, NULL, NULL),
(92, '335080002', 'Klerek', NULL, NULL, NULL, NULL, NULL),
(93, '335080074', 'Klopogading', NULL, NULL, NULL, NULL, NULL),
(94, '335080056', 'Kowang', NULL, NULL, NULL, NULL, NULL),
(95, '335080023', 'Krai', NULL, NULL, NULL, NULL, NULL),
(96, '335080096', 'Krumbang I', NULL, NULL, NULL, NULL, NULL),
(97, '335080295', 'Krumbang II', NULL, NULL, NULL, NULL, NULL),
(98, '335080161', 'Krumbang III', NULL, NULL, NULL, NULL, NULL),
(99, '335080097', 'Krumbang IV', NULL, NULL, NULL, NULL, NULL),
(100, '335080152', 'Kuburan', NULL, NULL, NULL, NULL, NULL),
(101, '335080101', 'Lalangan', NULL, NULL, NULL, NULL, NULL),
(102, '335080242', 'Lancing', NULL, NULL, NULL, NULL, NULL),
(103, '335080138', 'Langgar I', NULL, NULL, NULL, NULL, NULL),
(104, '335080123', 'Langgar II', NULL, NULL, NULL, NULL, NULL),
(105, '335080073', 'Langgar III / BT', NULL, NULL, NULL, NULL, NULL),
(106, '335080190', 'Langkapan', NULL, NULL, NULL, NULL, NULL),
(107, '335080243', 'Lateng I', NULL, NULL, NULL, NULL, NULL),
(108, '335080055', 'Lateng II', NULL, NULL, NULL, NULL, NULL),
(109, '335080142', 'Legenan', NULL, NULL, NULL, NULL, NULL),
(110, '335080159', 'Lewung I', NULL, NULL, NULL, NULL, NULL),
(111, '335080112', 'Lewung II', NULL, NULL, NULL, NULL, NULL),
(112, '335080038', 'Lobang Kanan', NULL, NULL, NULL, NULL, NULL),
(113, '335080008', 'Lobang Kiri', NULL, NULL, NULL, NULL, NULL),
(114, '335080120', 'Lombok', NULL, NULL, NULL, NULL, NULL),
(115, '335080244', 'Mangun', NULL, NULL, NULL, NULL, NULL),
(116, '335080172', 'Mian', NULL, NULL, NULL, NULL, NULL),
(117, '335080113', 'Mualap', NULL, NULL, NULL, NULL, NULL),
(118, '335080205', 'Ngrepyak', NULL, NULL, NULL, NULL, NULL),
(119, '335080155', 'Pahlewen', NULL, NULL, NULL, NULL, NULL),
(120, '-', 'Pak Duan', NULL, NULL, NULL, NULL, NULL),
(121, '335080126', 'Pak Jum', NULL, NULL, NULL, NULL, NULL),
(122, '335080193', 'Pakel I', NULL, NULL, NULL, NULL, NULL),
(123, '335080104', 'Pakel II', NULL, NULL, NULL, NULL, NULL),
(124, '335080089', 'Pakurejo', NULL, NULL, NULL, NULL, NULL),
(125, '335080025', 'Paleran', NULL, NULL, NULL, NULL, NULL),
(126, '335080185', 'Pancing I', NULL, NULL, NULL, NULL, NULL),
(127, '335080250', 'Pancing II', NULL, NULL, NULL, NULL, NULL),
(128, '335080249', 'Pancing III', NULL, NULL, NULL, NULL, NULL),
(129, '335080248', 'Pancing IV', NULL, NULL, NULL, NULL, NULL),
(130, '335080066', 'Pandanwangi I', NULL, NULL, NULL, NULL, NULL),
(131, '335080017', 'Pandanwangi II', NULL, NULL, NULL, NULL, NULL),
(132, '335080090', 'Panggung', NULL, NULL, NULL, NULL, NULL),
(133, '335080111', 'Pelus I', NULL, NULL, NULL, NULL, NULL),
(134, '335080114', 'Pelus II', NULL, NULL, NULL, NULL, NULL),
(135, '-', 'Pelus III', NULL, NULL, NULL, NULL, NULL),
(136, '335080252', 'Petehan', NULL, NULL, NULL, NULL, NULL),
(137, '-', 'Pinus', NULL, NULL, NULL, NULL, NULL),
(138, '335080102', 'Poh I', NULL, NULL, NULL, NULL, NULL),
(139, '335080186', 'Poh II', NULL, NULL, NULL, NULL, NULL),
(140, '335080254', 'Poh III', NULL, NULL, NULL, NULL, NULL),
(141, '335080247', 'Poh IV', NULL, NULL, NULL, NULL, NULL),
(142, '335080246', 'Poh V', NULL, NULL, NULL, NULL, NULL),
(143, '335080253', 'Poh VI', NULL, NULL, NULL, NULL, NULL),
(144, '335080255', 'Ponco Kusumo', NULL, NULL, NULL, NULL, NULL),
(145, '-', 'Prapatan', NULL, NULL, NULL, NULL, NULL),
(146, '335080034', 'Pringtali', NULL, NULL, NULL, NULL, NULL),
(147, '335080092', 'Rahayu', NULL, NULL, NULL, NULL, NULL),
(148, '335080037', 'Rakinten', NULL, NULL, NULL, NULL, NULL),
(149, '335080020', 'Rawaan', NULL, NULL, NULL, NULL, NULL),
(150, '335080080', 'Regoyo', NULL, NULL, NULL, NULL, NULL),
(151, '335080012', 'Rejali', NULL, NULL, NULL, NULL, NULL),
(152, '335080176', 'Rekesan I', NULL, NULL, NULL, NULL, NULL),
(153, '335080257', 'Rekesan II', NULL, NULL, NULL, NULL, NULL),
(154, '335080115', 'Repeh', NULL, NULL, NULL, NULL, NULL),
(155, '335080258', 'Rowo Asin', NULL, NULL, NULL, NULL, NULL),
(156, '335080004', 'Rowo Gedang', NULL, NULL, NULL, NULL, NULL),
(157, '335080259', 'Sadap Sentul', NULL, NULL, NULL, NULL, NULL),
(158, '335080079', 'Sapari', NULL, NULL, NULL, NULL, NULL),
(159, '335080039', 'Sarijo', NULL, NULL, NULL, NULL, NULL),
(160, '335080029', 'Selok Awar-awar', NULL, NULL, NULL, NULL, NULL),
(161, '335080014', 'Selokambang ', NULL, NULL, NULL, NULL, NULL),
(162, '335080005', 'Sempu I', NULL, NULL, NULL, NULL, NULL),
(163, '335080077', 'Sempu II', NULL, NULL, NULL, NULL, NULL),
(164, '335080147', 'Semut I', NULL, NULL, NULL, NULL, NULL),
(165, '335080154', 'Semut II', NULL, NULL, NULL, NULL, NULL),
(166, '335080171', 'Semut III', NULL, NULL, NULL, NULL, NULL),
(167, '335080144', 'Sengon', NULL, NULL, NULL, NULL, NULL),
(168, '335080040', 'Solekan', NULL, NULL, NULL, NULL, NULL),
(169, '335080011', 'Soponyono', NULL, NULL, NULL, NULL, NULL),
(170, '335080068', 'Srenteng', NULL, NULL, NULL, NULL, NULL),
(171, '335080105', 'Suco', NULL, NULL, NULL, NULL, NULL),
(172, '335080035', 'Sukorejo', NULL, NULL, NULL, NULL, NULL),
(173, '335080270', 'Sumber Banji', NULL, NULL, NULL, NULL, NULL),
(174, '335080263', 'Sumber Baung', NULL, NULL, NULL, NULL, NULL),
(175, '335080036', 'Sumber Bendo', NULL, NULL, NULL, NULL, NULL),
(176, '335080183', 'Sumber Bening', NULL, NULL, NULL, NULL, NULL),
(177, '335080262', 'Sumber Bentrong', NULL, NULL, NULL, NULL, NULL),
(178, '335080214', 'Sumber Berca', NULL, NULL, NULL, NULL, NULL),
(179, '335080210', 'Sumber Biting', NULL, NULL, NULL, NULL, NULL),
(180, '335080093', 'Sumber Bopong', NULL, NULL, NULL, NULL, NULL),
(181, '335080071', 'Sumber Brubi', NULL, NULL, NULL, NULL, NULL),
(182, '335080109', 'Sumber Bulak Wareng', NULL, NULL, NULL, NULL, NULL),
(183, '335080148', 'Sumber Bulus', NULL, NULL, NULL, NULL, NULL),
(184, '335080264', 'Sumber Bunting', NULL, NULL, NULL, NULL, NULL),
(185, '335080146', 'Sumber Buntung ', NULL, NULL, NULL, NULL, NULL),
(186, '335080266', 'Sumber Buntung II', NULL, NULL, NULL, NULL, NULL),
(187, '335080203', 'Sumber Buntung III', NULL, NULL, NULL, NULL, NULL),
(188, '335080195', 'Sumber Bunyu ', NULL, NULL, NULL, NULL, NULL),
(189, '335080094', 'Sumber Cabek I', NULL, NULL, NULL, NULL, NULL),
(190, '335080216', 'Sumber Cabek II', NULL, NULL, NULL, NULL, NULL),
(191, '335080225', 'Sumber Cemeng', NULL, NULL, NULL, NULL, NULL),
(192, '335080268', 'Sumber Cilik', NULL, NULL, NULL, NULL, NULL),
(193, '335080261', 'Sumber Ciri', NULL, NULL, NULL, NULL, NULL),
(194, '335080269', 'Sumber Dadi', NULL, NULL, NULL, NULL, NULL),
(195, '-', 'Sumber Dairun', NULL, NULL, NULL, NULL, NULL),
(196, '335080271', 'Sumber Dluwuk', NULL, NULL, NULL, NULL, NULL),
(197, '335080272', 'Sumber Dongki', NULL, NULL, NULL, NULL, NULL),
(198, '335080273', 'Sumber Dukuh', NULL, NULL, NULL, NULL, NULL),
(199, '335080135', 'Sumber Gebang', NULL, NULL, NULL, NULL, NULL),
(200, '335080207', 'Sumber Gebang', NULL, NULL, NULL, NULL, NULL),
(201, '335080058', 'Sumber Gebang', NULL, NULL, NULL, NULL, NULL),
(202, '-', 'Sumber Gedong Sutro', NULL, NULL, NULL, NULL, NULL),
(203, '335080060', 'Sumber Gendol', NULL, NULL, NULL, NULL, NULL),
(204, '335080033', 'Sumber Gentong', NULL, NULL, NULL, NULL, NULL),
(205, '335080162', 'Sumber Gentong', NULL, NULL, NULL, NULL, NULL),
(206, '335080303', 'Sumber Gentong', NULL, NULL, NULL, NULL, NULL),
(207, '335080191', 'Sumber Gledek', NULL, NULL, NULL, NULL, NULL),
(208, '335080132', 'Sumber Glendangan', NULL, NULL, NULL, NULL, NULL),
(209, '335080046', 'Sumber Glintungan', NULL, NULL, NULL, NULL, NULL),
(210, '335080018', 'Sumber Gogosan', NULL, NULL, NULL, NULL, NULL),
(211, '335080202', 'Sumber Gotehan', NULL, NULL, NULL, NULL, NULL),
(212, '335080116', 'Sumber Grimis', NULL, NULL, NULL, NULL, NULL),
(213, '335080098', 'Sumber Guci', NULL, NULL, NULL, NULL, NULL),
(214, '335080156', 'Sumber Gumuk Mas', NULL, NULL, NULL, NULL, NULL),
(215, '335080200', 'Sumber Jagal', NULL, NULL, NULL, NULL, NULL),
(216, '335080106', 'Sumber Jajang I', NULL, NULL, NULL, NULL, NULL),
(217, '335080140', 'Sumber Jajang II', NULL, NULL, NULL, NULL, NULL),
(218, '335080081', 'Sumber Jajang III', NULL, NULL, NULL, NULL, NULL),
(219, '335080049', 'Sumber Jambe', NULL, NULL, NULL, NULL, NULL),
(220, '335080168', 'Sumber Jebok', NULL, NULL, NULL, NULL, NULL),
(221, '335080032', 'Sumber Jengger', NULL, NULL, NULL, NULL, NULL),
(222, '335080137', 'Sumber Kadi', NULL, NULL, NULL, NULL, NULL),
(223, '335080275', 'Sumber Kajar Kuning', NULL, NULL, NULL, NULL, NULL),
(224, '335080076', 'Sumber Kaliputih', NULL, NULL, NULL, NULL, NULL),
(225, '335080277', 'Sumber Kalong', NULL, NULL, NULL, NULL, NULL),
(226, '335080278', 'Sumber Kalong', NULL, NULL, NULL, NULL, NULL),
(227, '335080103', 'Sumber Kamar A', NULL, NULL, NULL, NULL, NULL),
(228, '335080279', 'Sumber Kecambil', NULL, NULL, NULL, NULL, NULL),
(229, '335080201', 'Sumber Kedawung', NULL, NULL, NULL, NULL, NULL),
(230, '-', 'Sumber Kemanten', NULL, NULL, NULL, NULL, NULL),
(231, '335080189', 'Sumber Kembang', NULL, NULL, NULL, NULL, NULL),
(232, '335080160', 'Sumber Kembar', NULL, NULL, NULL, NULL, NULL),
(233, '-', 'Sumber Kenek', NULL, NULL, NULL, NULL, NULL),
(234, '335080048', 'Sumber Kepuh', NULL, NULL, NULL, NULL, NULL),
(235, '335080053', 'Sumber Khutuk', NULL, NULL, NULL, NULL, NULL),
(236, '335080127', 'Sumber Klinting', NULL, NULL, NULL, NULL, NULL),
(237, '335080136', 'Sumber Klutuk', NULL, NULL, NULL, NULL, NULL),
(238, '335080276', 'Sumber Kokap', NULL, NULL, NULL, NULL, NULL),
(239, '335080133', 'Sumber Kolbek', NULL, NULL, NULL, NULL, NULL),
(240, '335080280', 'Sumber Kramat', NULL, NULL, NULL, NULL, NULL),
(241, '335080095', 'Sumber Kuning', NULL, NULL, NULL, NULL, NULL),
(242, '335080015', 'Sumber Kutuk', NULL, NULL, NULL, NULL, NULL),
(243, '335080281', 'Sumber Ledok', NULL, NULL, NULL, NULL, NULL),
(244, '335080282', 'Sumber Lengkong I', NULL, NULL, NULL, NULL, NULL),
(245, '335080175', 'Sumber Lengkong II', NULL, NULL, NULL, NULL, NULL),
(246, '335080163', 'Sumber Logong', NULL, NULL, NULL, NULL, NULL),
(247, '335080164', 'Sumber Lowo', NULL, NULL, NULL, NULL, NULL),
(248, '-', 'Sumber Manggarai', NULL, NULL, NULL, NULL, NULL),
(249, '335080194', 'Sumber Mijah', NULL, NULL, NULL, NULL, NULL),
(250, '335080283', 'Sumber Mojo', NULL, NULL, NULL, NULL, NULL),
(251, '335080284', 'Sumber Mualam', NULL, NULL, NULL, NULL, NULL),
(252, '335080022', 'Sumber Mujur', NULL, NULL, NULL, NULL, NULL),
(253, '335080285', 'Sumber Munder', NULL, NULL, NULL, NULL, NULL),
(254, '335080099', 'Sumber Muris', NULL, NULL, NULL, NULL, NULL),
(255, '-', 'Sumber Nah', NULL, NULL, NULL, NULL, NULL),
(256, '335080199', 'Sumber Ngambon', NULL, NULL, NULL, NULL, NULL),
(257, '335080128', 'Sumber Nongko I', NULL, NULL, NULL, NULL, NULL),
(258, '335080177', 'Sumber Nongko II', NULL, NULL, NULL, NULL, NULL),
(259, '335080286', 'Sumber Ojo Lali', NULL, NULL, NULL, NULL, NULL),
(260, '335080314', 'Sumber Onggomanik', NULL, NULL, NULL, NULL, NULL),
(261, '335080100', 'Sumber Pakel', NULL, NULL, NULL, NULL, NULL),
(262, '335080062', 'Sumber Pakem', NULL, NULL, NULL, NULL, NULL),
(263, '335080047', 'Sumber Pakis', NULL, NULL, NULL, NULL, NULL),
(264, '335080251', 'Sumber Pancoran', NULL, NULL, NULL, NULL, NULL),
(265, '335080212', 'Sumber Pandan', NULL, NULL, NULL, NULL, NULL),
(266, '335080287', 'Sumber Parang', NULL, NULL, NULL, NULL, NULL),
(267, '335080288', 'Sumber Penanggungan', NULL, NULL, NULL, NULL, NULL),
(268, '335080204', 'Sumber Penggung', NULL, NULL, NULL, NULL, NULL),
(269, '335080304', 'Sumber Pentung Sumur', NULL, NULL, NULL, NULL, NULL),
(270, '335080085', 'Sumber Persam', NULL, NULL, NULL, NULL, NULL),
(271, '335080211', 'Sumber Petung', NULL, NULL, NULL, NULL, NULL),
(272, '335080289', 'Sumber Poh', NULL, NULL, NULL, NULL, NULL),
(273, '335080179', 'Sumber Pule', NULL, NULL, NULL, NULL, NULL),
(274, '335080030', 'Sumber Puring', NULL, NULL, NULL, NULL, NULL),
(275, '335080063', 'Sumber Putri', NULL, NULL, NULL, NULL, NULL),
(276, '335080256', 'Sumber Rambak Pakis', NULL, NULL, NULL, NULL, NULL),
(277, '335080290', 'Sumber Rampal', NULL, NULL, NULL, NULL, NULL),
(278, '335080274', 'Sumber Ranu Glabak', NULL, NULL, NULL, NULL, NULL),
(279, '-', 'Sumber Ranu Wurung', NULL, NULL, NULL, NULL, NULL),
(280, '335080292', 'Sumber Ranuyoso', NULL, NULL, NULL, NULL, NULL),
(281, '335080291', 'Sumber Rojo', NULL, NULL, NULL, NULL, NULL),
(282, '335080173', 'Sumber Rowo', NULL, NULL, NULL, NULL, NULL),
(283, '335080192', 'Sumber Rowo', NULL, NULL, NULL, NULL, NULL),
(284, '335080293', 'Sumber Rowo Baung', NULL, NULL, NULL, NULL, NULL),
(285, '335080213', 'Sumber Rowo Kancu', NULL, NULL, NULL, NULL, NULL),
(286, '335080294', 'Sumber Rowosumo', NULL, NULL, NULL, NULL, NULL),
(287, '335080121', 'Sumber Salak', NULL, NULL, NULL, NULL, NULL),
(288, '-', 'Sumber Sari', NULL, NULL, NULL, NULL, NULL),
(289, '335080041', 'Sumber Sayang', NULL, NULL, NULL, NULL, NULL),
(290, '335080260', 'Sumber Sebono', NULL, NULL, NULL, NULL, NULL),
(291, '335080118', 'Sumber Sengon', NULL, NULL, NULL, NULL, NULL),
(292, '335080296', 'Sumber Sentul', NULL, NULL, NULL, NULL, NULL),
(293, '335080297', 'Sumber Sepikul', NULL, NULL, NULL, NULL, NULL),
(294, '335080298', 'Sumber Sinap', NULL, NULL, NULL, NULL, NULL),
(295, '335080019', 'Sumber Sinonggo', NULL, NULL, NULL, NULL, NULL),
(296, '335080198', 'Sumber Sintok', NULL, NULL, NULL, NULL, NULL),
(297, '335080299', 'Sumber Sonok', NULL, NULL, NULL, NULL, NULL),
(298, '335080091', 'Sumber Suko', NULL, NULL, NULL, NULL, NULL),
(299, '335080208', 'Sumber Suko', NULL, NULL, NULL, NULL, NULL),
(300, '-', 'Sumber Sukosari', NULL, NULL, NULL, NULL, NULL),
(301, '335080300', 'Sumber Sumberan', NULL, NULL, NULL, NULL, NULL),
(302, '335080057', 'Sumber Takir', NULL, NULL, NULL, NULL, NULL),
(303, '335080153', 'Sumber Tangis', NULL, NULL, NULL, NULL, NULL),
(304, '335080301', 'Sumber Tangkil', NULL, NULL, NULL, NULL, NULL),
(305, '335080206', 'Sumber Tekik', NULL, NULL, NULL, NULL, NULL),
(306, '335080182', 'Sumber Tembok', NULL, NULL, NULL, NULL, NULL),
(307, '335080119', 'Sumber Temo', NULL, NULL, NULL, NULL, NULL),
(308, '335080302', 'Sumber Tempuran', NULL, NULL, NULL, NULL, NULL),
(309, '335080110', 'Sumber Tretes', NULL, NULL, NULL, NULL, NULL),
(310, '-', 'Sumber Tumpuk/Tunjung', NULL, NULL, NULL, NULL, NULL),
(311, '335080167', 'Sumber Umbulan', NULL, NULL, NULL, NULL, NULL),
(312, '335080305', 'Sumber Urang', NULL, NULL, NULL, NULL, NULL),
(313, '335080158', 'Sumber Urip', NULL, NULL, NULL, NULL, NULL),
(314, '335080180', 'Sumber Wakap', NULL, NULL, NULL, NULL, NULL),
(315, '335080307', 'Sumber Wongso', NULL, NULL, NULL, NULL, NULL),
(316, '335080308', 'Sumber Wringin', NULL, NULL, NULL, NULL, NULL),
(317, '335080054', 'Sumberan', NULL, NULL, NULL, NULL, NULL),
(318, '335080084', 'Tabon', NULL, NULL, NULL, NULL, NULL),
(319, '335080026', 'Talang', NULL, NULL, NULL, NULL, NULL),
(320, '335080124', 'Tambuh', NULL, NULL, NULL, NULL, NULL),
(321, '335080309', 'Tandak', NULL, NULL, NULL, NULL, NULL),
(322, '335080313', 'Temari', NULL, NULL, NULL, NULL, NULL),
(323, '-', 'Tino', NULL, NULL, NULL, NULL, NULL),
(324, '335080067', 'Tong Maling', NULL, NULL, NULL, NULL, NULL),
(325, '335080188', 'Umbul Sari I ', NULL, NULL, NULL, NULL, NULL),
(326, '335080044', 'Umbul Sari II', NULL, NULL, NULL, NULL, NULL),
(327, '335080306', 'Wakap', NULL, NULL, NULL, NULL, NULL),
(328, '335080311', 'Wareng', NULL, NULL, NULL, NULL, NULL),
(329, '335080312', 'Wonokerto', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_jenissaluran`
--

CREATE TABLE `m_jenissaluran` (
  `intIDJenisSaluran` int(11) DEFAULT NULL,
  `txtJenisSaluran` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_jenissaluran`
--

INSERT INTO `m_jenissaluran` (`intIDJenisSaluran`, `txtJenisSaluran`) VALUES
(1, 'Primer'),
(2, 'Sekunder'),
(3, 'Tersier');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenissaluran_bangunan`
--

CREATE TABLE `m_jenissaluran_bangunan` (
  `intIDJenisSaluranBangunan` int(11) NOT NULL,
  `txtJenisSaluranBangunan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_jenissaluran_bangunan`
--

INSERT INTO `m_jenissaluran_bangunan` (`intIDJenisSaluranBangunan`, `txtJenisSaluranBangunan`) VALUES
(1, 'Sekunder'),
(2, 'Tersier Kanan'),
(3, 'Tersier Kiri');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenissurvey`
--

CREATE TABLE `m_jenissurvey` (
  `intiDSurvey` int(11) NOT NULL,
  `txtSurvey` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_jenissurvey`
--

INSERT INTO `m_jenissurvey` (`intiDSurvey`, `txtSurvey`) VALUES
(1, 'Curah Hujan'),
(2, 'Pintu Air'),
(3, 'Data Tanaman'),
(4, 'Kerusakan Tanaman');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenistanaman`
--

CREATE TABLE `m_jenistanaman` (
  `intIDJenisTanaman` int(11) NOT NULL,
  `txtKodeSingkatan` varchar(50) NOT NULL DEFAULT '0',
  `txtJenisTanaman` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_jenistanaman`
--

INSERT INTO `m_jenistanaman` (`intIDJenisTanaman`, `txtKodeSingkatan`, `txtJenisTanaman`) VALUES
(1, 'P01', 'Padi'),
(2, 'P02', 'Tebu');

-- --------------------------------------------------------

--
-- Table structure for table `m_kecamatan`
--

CREATE TABLE `m_kecamatan` (
  `intIDKecamatan` int(11) NOT NULL,
  `txtKecamatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_kecamatan`
--

INSERT INTO `m_kecamatan` (`intIDKecamatan`, `txtKecamatan`) VALUES
(3508010, 'TEMPURSARI'),
(3508020, 'PRONOJIWO'),
(3508030, 'CANDIPURO'),
(3508040, 'PASIRIAN'),
(3508050, 'TEMPEH'),
(3508060, 'LUMAJANG'),
(3508061, 'SUMBERSUKO'),
(3508070, 'TEKUNG'),
(3508080, 'KUNIR'),
(3508090, 'YOSOWILANGUN'),
(3508100, 'ROWOKANGKUNG'),
(3508110, 'JATIROTO'),
(3508120, 'RANDUAGUNG'),
(3508130, 'SUKODONO'),
(3508140, 'PADANG'),
(3508150, 'PASRUJAMBE'),
(3508160, 'SENDURO'),
(3508170, 'GUCIALIT'),
(3508180, 'KEDUNGJAJANG'),
(3508190, 'KLAKAH'),
(3508200, 'RANUYOSO');

-- --------------------------------------------------------

--
-- Table structure for table `m_kewenangan_jaringan`
--

CREATE TABLE `m_kewenangan_jaringan` (
  `intIDKewenanganJaringan` int(11) NOT NULL,
  `txtKewenanganJaringan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_kewenangan_jaringan`
--

INSERT INTO `m_kewenangan_jaringan` (`intIDKewenanganJaringan`, `txtKewenanganJaringan`) VALUES
(1, 'Provinsi'),
(2, 'Kabupaten');

-- --------------------------------------------------------

--
-- Table structure for table `m_penyebabkerusakan`
--

CREATE TABLE `m_penyebabkerusakan` (
  `intIDPenyebabKerusakan` int(11) NOT NULL,
  `txtKodeSingkatan` varchar(50) NOT NULL DEFAULT '0',
  `txtPenyebabKerusakan` varchar(75) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_penyebabkerusakan`
--

INSERT INTO `m_penyebabkerusakan` (`intIDPenyebabKerusakan`, `txtKodeSingkatan`, `txtPenyebabKerusakan`) VALUES
(1, 'K01', 'Kekeringan'),
(2, 'K02', 'Hama / Penyakit'),
(3, 'K03', 'Kebanjiran');

-- --------------------------------------------------------

--
-- Table structure for table `m_programintensifikasi`
--

CREATE TABLE `m_programintensifikasi` (
  `intIDProgramIntensifikasi` int(11) NOT NULL,
  `txtKodeSingkatan` varchar(50) NOT NULL DEFAULT '0',
  `txtProgramIntensifikasi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_programintensifikasi`
--

INSERT INTO `m_programintensifikasi` (`intIDProgramIntensifikasi`, `txtKodeSingkatan`, `txtProgramIntensifikasi`) VALUES
(1, 'C01', 'Program Intensifikasi 1'),
(2, 'C02', 'Program Intensifikasi 2'),
(3, 'C03', 'Program Intensifikasi 3');

-- --------------------------------------------------------

--
-- Table structure for table `m_saluran`
--

CREATE TABLE `m_saluran` (
  `intIDSaluran` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `txtKodeSingkatan` varchar(50) DEFAULT NULL,
  `txtSaluran` varchar(50) DEFAULT NULL,
  `intIDJenisSaluran` int(11) DEFAULT NULL,
  `intIDSaluranAsal` int(11) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL,
  `dblBakuSawah` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_saluran`
--

INSERT INTO `m_saluran` (`intIDSaluran`, `intIDJaringanIrigasi`, `txtKodeSingkatan`, `txtSaluran`, `intIDJenisSaluran`, `intIDSaluranAsal`, `dblLatitude`, `dblLongitude`, `dblBakuSawah`) VALUES
(1, 1, 'TK001', 'Tekung', 1, 0, NULL, NULL, NULL),
(2, 1, 'TK002', 'Tekung II', 2, 1, NULL, NULL, NULL),
(3, 1, 'TK003', 'T BTK 7 Kiri', 3, 2, NULL, NULL, NULL),
(4, 1, 'TK004', 'T BTK 7 Kiri Tengah', 3, 2, NULL, NULL, NULL),
(5, 1, 'TK005', 'T BTK 7 Kanan', 3, 2, NULL, NULL, NULL),
(6, 2, 'BC01', 'Bacem Primer', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_stasiuncurahhujan`
--

CREATE TABLE `m_stasiuncurahhujan` (
  `intIDStasiunCurahHujan` int(11) NOT NULL,
  `intIDKecamatan` int(11) DEFAULT NULL,
  `txtStasiunCurahHujan` varchar(50) DEFAULT NULL,
  `txtNo` varchar(50) DEFAULT NULL,
  `txtElevasi` varchar(50) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL,
  `isAreaStrict` int(11) DEFAULT 1 COMMENT '1 = strict (harus berada di dalam area) , 0 = bebas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_stasiuncurahhujan`
--

INSERT INTO `m_stasiuncurahhujan` (`intIDStasiunCurahHujan`, `intIDKecamatan`, `txtStasiunCurahHujan`, `txtNo`, `txtElevasi`, `dblLatitude`, `dblLongitude`, `isAreaStrict`) VALUES
(1, 3508030, 'Stasiun 3', '3', '200', -8.12653400, 113.14383300, 1),
(2, 3508030, 'Stasiun 2', '2', '300', -7.78911790, 110.36354860, 1),
(3, 3508160, 'Tes Sawojajar', '987', '100', -7.96940350, 112.65880260, 1),
(4, 3508060, 'Moh. Yamin', 'MY1504', '500', -8.13309520, 113.22623290, 1),
(5, 3508060, 'Kantor PUTR', 'PUTR', '543', -8.13411930, 113.22618260, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_upt`
--

CREATE TABLE `m_upt` (
  `intIDUPT` int(11) NOT NULL,
  `txtNamaUPT` varchar(50) DEFAULT NULL,
  `intIDKecamatan` int(11) DEFAULT NULL,
  `txtAlamat` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_upt`
--

INSERT INTO `m_upt` (`intIDUPT`, `txtNamaUPT`, `intIDKecamatan`, `txtAlamat`) VALUES
(1, 'UPT 1', 3508010, 'JL Margonda No 30');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `intIDuser` int(11) NOT NULL,
  `txtUsername` varchar(50) DEFAULT NULL,
  `txtEmail` varchar(50) DEFAULT NULL,
  `txtPassword` varchar(50) DEFAULT NULL,
  `txtFirstName` varchar(50) DEFAULT NULL,
  `txtLastName` varchar(50) DEFAULT NULL,
  `txtAddress` varchar(100) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL COMMENT '0 = NonAktif , 1 = aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`intIDuser`, `txtUsername`, `txtEmail`, `txtPassword`, `txtFirstName`, `txtLastName`, `txtAddress`, `isActive`) VALUES
(1, 'admin', 'admin@gmail.com', '+kmgTDi12/wP8iQGZWbhqA==', 'admin', 'last name admin', 'jalan merdeka no 45', 1),
(9, 'superuser', 'superuser@gmail.com', 'jmw4KLMjLjXGIBL+6fMXHg==', 'Super', 'User', 'Jl. Danau Sentani Dalam H1L4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_admin`
--

CREATE TABLE `m_user_admin` (
  `intIDuser` int(11) NOT NULL,
  `txtNamaDepan` varchar(100) DEFAULT NULL,
  `txtNamaBelakang` varchar(100) DEFAULT NULL,
  `txtUsername` varchar(50) DEFAULT NULL,
  `txtPassword` varchar(50) DEFAULT NULL,
  `intStatus` int(11) DEFAULT NULL,
  `dtRegistrasi` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_user_admin`
--

INSERT INTO `m_user_admin` (`intIDuser`, `txtNamaDepan`, `txtNamaBelakang`, `txtUsername`, `txtPassword`, `intStatus`, `dtRegistrasi`) VALUES
(1, 'admin', 'admin', 'admin', '123456', 1, '2019-11-11 20:40:54.000000');

-- --------------------------------------------------------

--
-- Table structure for table `m_user_akses_jaringanirigasi`
--

CREATE TABLE `m_user_akses_jaringanirigasi` (
  `intIDAksesJaringanIrigasi` int(11) NOT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_user_akses_jaringanirigasi`
--

INSERT INTO `m_user_akses_jaringanirigasi` (`intIDAksesJaringanIrigasi`, `intIDUser`, `intIDJaringanIrigasi`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_akses_stasiuncurahhujan`
--

CREATE TABLE `m_user_akses_stasiuncurahhujan` (
  `intIDAksesStasiunCurahHujan` int(11) NOT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `intIDStasiunCurahHujan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `m_user_akses_stasiuncurahhujan`
--

INSERT INTO `m_user_akses_stasiuncurahhujan` (`intIDAksesStasiunCurahHujan`, `intIDUser`, `intIDStasiunCurahHujan`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tr_surveycurahhujan`
--

CREATE TABLE `tr_surveycurahhujan` (
  `intIDCurahHujan` int(11) NOT NULL,
  `intIDKecamatan` int(11) DEFAULT NULL,
  `intIDStasiunCurahHujan` int(11) DEFAULT NULL,
  `dblCurahHujan` double(11,2) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL,
  `dtSurvey` datetime(6) DEFAULT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `dtUpdate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tr_surveycurahhujan`
--

INSERT INTO `tr_surveycurahhujan` (`intIDCurahHujan`, `intIDKecamatan`, `intIDStasiunCurahHujan`, `dblCurahHujan`, `dblLatitude`, `dblLongitude`, `dtSurvey`, `intIDUser`, `dtUpdate`) VALUES
(1, 3508030, 1, 20.00, 999.99999999, 999.99999999, '2019-11-20 04:46:54.000000', 1, NULL),
(2, 3508030, 1, 50.00, 999.99999999, 999.99999999, '2019-11-14 04:46:54.000000', 1, NULL),
(3, 3508030, 1, 60.00, 999.99999999, 999.99999999, '2019-11-07 04:46:54.000000', 1, NULL),
(4, 3508160, 3, 55.00, -7.96939640, 112.65886100, '2019-11-24 20:16:43.000000', 1, NULL),
(7, 3508060, 2, 26.00, -3.98474800, 43.97477300, '2020-12-24 20:16:43.000000', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_surveykerusakantanaman`
--

CREATE TABLE `tr_surveykerusakantanaman` (
  `intIDSurveyKerusakanTanaman` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `intIDSaluran` int(11) DEFAULT 0,
  `dblBakuSawah` double(11,2) DEFAULT NULL,
  `intIDKecamatan` int(11) DEFAULT NULL,
  `intIDDesa` bigint(11) DEFAULT NULL,
  `intIDProgramIntensifikasi` int(11) DEFAULT NULL,
  `intIDPenyebabKerusakan` int(11) DEFAULT NULL,
  `dblMati` double(11,2) DEFAULT NULL,
  `dblPuso` double(11,2) DEFAULT NULL,
  `dblBerat` double(11,2) DEFAULT NULL,
  `dblSedang` double(11,2) DEFAULT NULL,
  `dblRingan` double(11,2) DEFAULT NULL,
  `dblLamaGenangan` double(11,2) DEFAULT NULL,
  `txtKeterangan` varchar(100) DEFAULT NULL,
  `dtSurvey` datetime(6) DEFAULT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `dtUpdate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tr_surveykerusakantanaman`
--

INSERT INTO `tr_surveykerusakantanaman` (`intIDSurveyKerusakanTanaman`, `intIDJaringanIrigasi`, `intIDSaluran`, `dblBakuSawah`, `intIDKecamatan`, `intIDDesa`, `intIDProgramIntensifikasi`, `intIDPenyebabKerusakan`, `dblMati`, `dblPuso`, `dblBerat`, `dblSedang`, `dblRingan`, `dblLamaGenangan`, `txtKeterangan`, `dtSurvey`, `intIDUser`, `dtUpdate`) VALUES
(12, 1, 1, 33.00, 3508030, 3508030001, 1, 1, 3.00, 1.00, 2.00, 8.00, 4.00, 2.00, 'ooja', '2019-11-17 15:43:35.000000', 1, NULL),
(13, 1, 1, 52.00, 3508060, 3508060017, 1, 2, 69.00, 65.00, 69.00, 69.00, 69.00, 66.00, 'bhq', '2019-12-01 18:47:15.000000', 1, NULL),
(21, 2, 2, 51.00, 3508060, 3508030001, 2, 1, 60.00, 55.00, 59.00, 68.00, 61.00, 62.00, 'iya aja', '2020-04-30 13:47:15.000000', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_surveypintuair`
--

CREATE TABLE `tr_surveypintuair` (
  `intIDSurveyPintuAir` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `intIDSaluran` int(11) DEFAULT 0,
  `dblBakuSawah` double(11,2) DEFAULT NULL,
  `Pelaksanaan_dblDebit` double(11,2) DEFAULT NULL,
  `Pelaksanaan_dblLPR` double(11,2) DEFAULT NULL,
  `Pelaksanaan_dblFPR` double(11,2) DEFAULT NULL,
  `Perencanaan_dblDebit` double(11,2) DEFAULT NULL,
  `Perencanaan_dblLPR` double(11,2) DEFAULT NULL,
  `Perencanaan_dblFPR` double(11,2) DEFAULT NULL,
  `dtSurvey` datetime(6) DEFAULT current_timestamp(6),
  `intIDUser` int(11) DEFAULT NULL,
  `dtUpdate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tr_surveypintuair`
--

INSERT INTO `tr_surveypintuair` (`intIDSurveyPintuAir`, `intIDJaringanIrigasi`, `intIDSaluran`, `dblBakuSawah`, `Pelaksanaan_dblDebit`, `Pelaksanaan_dblLPR`, `Pelaksanaan_dblFPR`, `Perencanaan_dblDebit`, `Perencanaan_dblLPR`, `Perencanaan_dblFPR`, `dtSurvey`, `intIDUser`, `dtUpdate`) VALUES
(33, 1, 1, 33.00, 33.00, 66.00, 99.00, 66.00, 99.00, 99.00, '2019-11-17 15:39:02.000000', 1, NULL),
(34, 1, 1, 52.00, 56.00, 22.00, 52.00, 85.00, 85.00, 55.00, '2019-12-01 18:33:57.000000', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_surveyrealisasitanaman`
--

CREATE TABLE `tr_surveyrealisasitanaman` (
  `intIDSurveyRealisasiTanaman` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `intIDSaluran` int(11) DEFAULT 0,
  `dblBakuSawah` double(11,2) DEFAULT NULL,
  `txtKeterangan` varchar(150) DEFAULT NULL,
  `Padi_MH_dblBibit` double(11,2) DEFAULT NULL,
  `Padi_MH_dblGarap` double(11,2) DEFAULT NULL,
  `Padi_MH_dblTanam` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduIjin_dblBibit` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduIjin_dblGarap` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduIjin_dblTanam` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduTidakIjin_dblBibit` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduTidakIjin_dblGarap` double(11,2) DEFAULT NULL,
  `Padi_MK1_GaduTidakIjin_dblTanam` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduIjin_dblBibit` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduIjin_dblGarap` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduIjin_dblTanam` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduTidakIjin_dblBibit` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduTidakIjin_dblGarap` double(11,2) DEFAULT NULL,
  `Padi_MK2_GaduTidakIjin_dblTanam` double(11,2) DEFAULT NULL,
  `Padi_Total` double(11,2) DEFAULT NULL,
  `TebuRakyat_dblCemplong` double(11,2) DEFAULT NULL,
  `TebuRakyat_dblBibit` double(11,2) DEFAULT NULL,
  `TebuRakyat_dblMuda` double(11,2) DEFAULT NULL,
  `TebuRakyat_dblTua` double(11,2) DEFAULT NULL,
  `TebuRakyat_dblTotal` double(11,2) DEFAULT NULL,
  `TebuPerusahaan_dblCemplong` double(11,2) DEFAULT NULL,
  `TebuPerusahaan_dblBibit` double(11,2) DEFAULT NULL,
  `TebuPerusahaan_dblMuda` double(11,2) DEFAULT NULL,
  `TebuPerusahaan_dblTua` double(11,2) DEFAULT NULL,
  `TebuPerusahaan_dblTotal` double(11,2) DEFAULT NULL,
  `Polowijo_MH` double(11,2) DEFAULT NULL,
  `Polowijo_MK1_Jagung` double(11,2) DEFAULT NULL,
  `Polowijo_MK1_Kedelai` double(11,2) DEFAULT NULL,
  `Polowijo_MK1_Lainnya` double(11,2) DEFAULT NULL,
  `Polowijo_MK2_Jagung` double(11,2) DEFAULT NULL,
  `Polowijo_MK2_Kedelai` double(11,2) DEFAULT NULL,
  `Polowijo_MK2_Lainnya` double(11,2) DEFAULT NULL,
  `Polowijo_Tembakau` double(11,2) DEFAULT NULL,
  `Polowijo_Rosela` double(11,2) DEFAULT NULL,
  `Polowijo_Total` double(11,2) DEFAULT NULL,
  `Bero_dblAsli` double(11,2) DEFAULT NULL,
  `Bero_dblPascaPanen` double(11,2) DEFAULT NULL,
  `Bero_Total` double(11,2) DEFAULT NULL,
  `dtSurvey` datetime(6) DEFAULT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `Debit_Intake` double(11,2) DEFAULT NULL,
  `Debit_IjinAir` double(11,2) DEFAULT NULL,
  `Debit_TotalTersier` double(11,2) DEFAULT NULL,
  `Debit_Efisiensi` double(11,2) DEFAULT NULL,
  `dblLPR` double(11,2) DEFAULT NULL,
  `dblFPR` double(11,2) DEFAULT NULL,
  `dblTotalLuasTanaman` double(11,2) DEFAULT NULL,
  `dtUpdate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tr_surveyrealisasitanaman`
--

INSERT INTO `tr_surveyrealisasitanaman` (`intIDSurveyRealisasiTanaman`, `intIDJaringanIrigasi`, `intIDSaluran`, `dblBakuSawah`, `txtKeterangan`, `Padi_MH_dblBibit`, `Padi_MH_dblGarap`, `Padi_MH_dblTanam`, `Padi_MK1_GaduIjin_dblBibit`, `Padi_MK1_GaduIjin_dblGarap`, `Padi_MK1_GaduIjin_dblTanam`, `Padi_MK1_GaduTidakIjin_dblBibit`, `Padi_MK1_GaduTidakIjin_dblGarap`, `Padi_MK1_GaduTidakIjin_dblTanam`, `Padi_MK2_GaduIjin_dblBibit`, `Padi_MK2_GaduIjin_dblGarap`, `Padi_MK2_GaduIjin_dblTanam`, `Padi_MK2_GaduTidakIjin_dblBibit`, `Padi_MK2_GaduTidakIjin_dblGarap`, `Padi_MK2_GaduTidakIjin_dblTanam`, `Padi_Total`, `TebuRakyat_dblCemplong`, `TebuRakyat_dblBibit`, `TebuRakyat_dblMuda`, `TebuRakyat_dblTua`, `TebuRakyat_dblTotal`, `TebuPerusahaan_dblCemplong`, `TebuPerusahaan_dblBibit`, `TebuPerusahaan_dblMuda`, `TebuPerusahaan_dblTua`, `TebuPerusahaan_dblTotal`, `Polowijo_MH`, `Polowijo_MK1_Jagung`, `Polowijo_MK1_Kedelai`, `Polowijo_MK1_Lainnya`, `Polowijo_MK2_Jagung`, `Polowijo_MK2_Kedelai`, `Polowijo_MK2_Lainnya`, `Polowijo_Tembakau`, `Polowijo_Rosela`, `Polowijo_Total`, `Bero_dblAsli`, `Bero_dblPascaPanen`, `Bero_Total`, `dtSurvey`, `intIDUser`, `Debit_Intake`, `Debit_IjinAir`, `Debit_TotalTersier`, `Debit_Efisiensi`, `dblLPR`, `dblFPR`, `dblTotalLuasTanaman`, `dtUpdate`) VALUES
(3, 1, 1, 33.00, NULL, 66.00, 58.00, 99.00, 66.00, 55.00, 88.00, 47.00, 66.00, 55.00, 33.00, 22.00, 11.00, 44.00, 33.00, 47.00, NULL, 77.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, NULL, '2019-11-17 15:40:14.000000', 1, NULL, NULL, NULL, NULL, 22.00, 22.00, 22.00, NULL),
(5, 1, 1, 30.00, NULL, 20.00, 10.00, 60.00, 12.00, 18.00, 12.00, 12.00, 12.00, 12.00, 15.00, 15.00, 6.00, 13.00, 12.00, 15.00, NULL, 18.00, 12.00, 13.00, 15.00, NULL, NULL, NULL, NULL, NULL, NULL, 18.00, 16.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15.00, 12.00, NULL, '2019-11-21 03:55:21.000000', 1, NULL, NULL, NULL, NULL, 18.00, 15.00, 18.00, NULL),
(6, 1, 1, 69.00, NULL, 55.00, 88.00, 8.00, 88.00, 8.00, 88.00, 8.00, 8.00, 88.00, 8.00, 88.00, 88.00, 88.00, 88.00, 88.00, NULL, 8.00, 88.00, 88.00, 8.00, NULL, NULL, NULL, NULL, NULL, NULL, 88.00, 88.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8.00, 8.00, NULL, '2019-12-01 18:34:47.000000', 1, NULL, NULL, NULL, NULL, 8.00, 888.00, 88.00, NULL),
(7, 1, 1, 20.00, 'test api', 50.00, 51.00, 52.00, 55.00, 56.00, 57.00, 60.00, 61.00, 62.00, 65.00, 66.00, 67.00, 70.00, 71.00, 72.00, 99.00, 75.00, 76.00, 77.00, 78.00, 99.00, 80.00, 81.00, 82.00, 83.00, 84.00, 85.00, 86.00, 87.00, 88.00, 90.00, 91.00, 92.00, 95.00, 96.00, 99.00, 91.00, 92.00, 99.00, '2019-11-17 15:40:14.000000', 1, 30.00, 31.00, 35.00, 32.00, 40.00, 41.00, 515.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_survey_bangunan`
--

CREATE TABLE `tr_survey_bangunan` (
  `intIDSurveyBangunan` int(11) NOT NULL,
  `intIDJaringanIrigasi` int(11) DEFAULT NULL,
  `intIDBangunan` int(11) DEFAULT NULL,
  `intIDJenisSaluran` int(11) DEFAULT NULL,
  `dblLatitude` double(11,8) DEFAULT NULL,
  `dblLongitude` double(11,8) DEFAULT NULL,
  `intKondisi` int(11) DEFAULT NULL COMMENT '1 = Baik, 2=Rusak Ringan, 3 = Rusak Sedang, 4 = Rusak Berat',
  `intFungsi` int(11) DEFAULT NULL COMMENT '1 = Berfungsi, 2= K, 3= Br, 4 = Tidak Berfungsi',
  `intMempengaruhiAliranAir` int(11) DEFAULT NULL COMMENT '1 = YA, 2= TIDAK',
  `txtFoto1` text DEFAULT NULL,
  `txtFoto2` text DEFAULT NULL,
  `txtFoto3` text DEFAULT NULL,
  `txtFoto4` text DEFAULT NULL,
  `txtFoto5` text DEFAULT NULL,
  `txtKeterangan` varchar(250) DEFAULT NULL,
  `dtSurvey` datetime(6) DEFAULT NULL,
  `intIDUser` int(11) DEFAULT NULL,
  `dtUpdate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tr_survey_bangunan`
--

INSERT INTO `tr_survey_bangunan` (`intIDSurveyBangunan`, `intIDJaringanIrigasi`, `intIDBangunan`, `intIDJenisSaluran`, `dblLatitude`, `dblLongitude`, `intKondisi`, `intFungsi`, `intMempengaruhiAliranAir`, `txtFoto1`, `txtFoto2`, `txtFoto3`, `txtFoto4`, `txtFoto5`, `txtKeterangan`, `dtSurvey`, `intIDUser`, `dtUpdate`) VALUES
(1, 1, 1, 1, -23.43634654, 999.99999999, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 01:02:00.000000', 1, NULL),
(4, 1, 2, 2, -7.96043430, 112.71955440, 0, 0, 0, '201911191328481.png', '201911191312312.png', '', '', '', 'hh', '2019-11-18 11:13:29.000000', 1, NULL),
(5, 1, 2, 2, -7.96043430, 112.71955440, 0, 0, 0, '201911191243531.png', '201911191243532.png', '201911191243533.png', '201911191243534.png', '201911191243535.png', 'iijj', '2019-11-19 12:43:53.000000', 1, NULL),
(6, 1, 2, 1, -7.78910480, 110.36356530, 1, 0, 1, '202004010345001.png', '202004010345001.png', '202004010345001.png', '202004010345001.png', '202004010345001.png', 'Oiya Aja', '2019-11-21 03:45:00.000000', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_tanamanrusak`
--

CREATE TABLE `tr_tanamanrusak` (
  `intIDTanamanRusak` int(11) NOT NULL,
  `intIDSurveyKerusakanTanaman` int(11) DEFAULT NULL,
  `intIDJenisTanaman` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tr_tanamanrusak`
--

INSERT INTO `tr_tanamanrusak` (`intIDTanamanRusak`, `intIDSurveyKerusakanTanaman`, `intIDJenisTanaman`) VALUES
(70, 12, 2),
(71, 12, 1),
(72, 13, 2),
(73, 21, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_bangunan_irigasi`
--
ALTER TABLE `m_bangunan_irigasi`
  ADD PRIMARY KEY (`intIDBangunan`) USING BTREE;

--
-- Indexes for table `m_desa`
--
ALTER TABLE `m_desa`
  ADD PRIMARY KEY (`intIDDesa`) USING BTREE;

--
-- Indexes for table `m_jaringanirigasi`
--
ALTER TABLE `m_jaringanirigasi`
  ADD PRIMARY KEY (`intIDJaringanIrigasi`) USING BTREE;

--
-- Indexes for table `m_jenissaluran_bangunan`
--
ALTER TABLE `m_jenissaluran_bangunan`
  ADD PRIMARY KEY (`intIDJenisSaluranBangunan`) USING BTREE;

--
-- Indexes for table `m_jenissurvey`
--
ALTER TABLE `m_jenissurvey`
  ADD PRIMARY KEY (`intiDSurvey`) USING BTREE;

--
-- Indexes for table `m_jenistanaman`
--
ALTER TABLE `m_jenistanaman`
  ADD PRIMARY KEY (`intIDJenisTanaman`) USING BTREE;

--
-- Indexes for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  ADD PRIMARY KEY (`intIDKecamatan`) USING BTREE;

--
-- Indexes for table `m_kewenangan_jaringan`
--
ALTER TABLE `m_kewenangan_jaringan`
  ADD PRIMARY KEY (`intIDKewenanganJaringan`) USING BTREE;

--
-- Indexes for table `m_penyebabkerusakan`
--
ALTER TABLE `m_penyebabkerusakan`
  ADD PRIMARY KEY (`intIDPenyebabKerusakan`) USING BTREE;

--
-- Indexes for table `m_programintensifikasi`
--
ALTER TABLE `m_programintensifikasi`
  ADD PRIMARY KEY (`intIDProgramIntensifikasi`) USING BTREE;

--
-- Indexes for table `m_saluran`
--
ALTER TABLE `m_saluran`
  ADD PRIMARY KEY (`intIDSaluran`) USING BTREE;

--
-- Indexes for table `m_stasiuncurahhujan`
--
ALTER TABLE `m_stasiuncurahhujan`
  ADD PRIMARY KEY (`intIDStasiunCurahHujan`) USING BTREE;

--
-- Indexes for table `m_upt`
--
ALTER TABLE `m_upt`
  ADD PRIMARY KEY (`intIDUPT`) USING BTREE;

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`intIDuser`) USING BTREE;

--
-- Indexes for table `m_user_admin`
--
ALTER TABLE `m_user_admin`
  ADD PRIMARY KEY (`intIDuser`) USING BTREE;

--
-- Indexes for table `m_user_akses_jaringanirigasi`
--
ALTER TABLE `m_user_akses_jaringanirigasi`
  ADD PRIMARY KEY (`intIDAksesJaringanIrigasi`) USING BTREE;

--
-- Indexes for table `m_user_akses_stasiuncurahhujan`
--
ALTER TABLE `m_user_akses_stasiuncurahhujan`
  ADD PRIMARY KEY (`intIDAksesStasiunCurahHujan`) USING BTREE;

--
-- Indexes for table `tr_surveycurahhujan`
--
ALTER TABLE `tr_surveycurahhujan`
  ADD PRIMARY KEY (`intIDCurahHujan`) USING BTREE;

--
-- Indexes for table `tr_surveykerusakantanaman`
--
ALTER TABLE `tr_surveykerusakantanaman`
  ADD PRIMARY KEY (`intIDSurveyKerusakanTanaman`) USING BTREE;

--
-- Indexes for table `tr_surveypintuair`
--
ALTER TABLE `tr_surveypintuair`
  ADD PRIMARY KEY (`intIDSurveyPintuAir`) USING BTREE;

--
-- Indexes for table `tr_surveyrealisasitanaman`
--
ALTER TABLE `tr_surveyrealisasitanaman`
  ADD PRIMARY KEY (`intIDSurveyRealisasiTanaman`) USING BTREE;

--
-- Indexes for table `tr_survey_bangunan`
--
ALTER TABLE `tr_survey_bangunan`
  ADD PRIMARY KEY (`intIDSurveyBangunan`) USING BTREE;

--
-- Indexes for table `tr_tanamanrusak`
--
ALTER TABLE `tr_tanamanrusak`
  ADD PRIMARY KEY (`intIDTanamanRusak`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_bangunan_irigasi`
--
ALTER TABLE `m_bangunan_irigasi`
  MODIFY `intIDBangunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_desa`
--
ALTER TABLE `m_desa`
  MODIFY `intIDDesa` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3508200012;

--
-- AUTO_INCREMENT for table `m_jaringanirigasi`
--
ALTER TABLE `m_jaringanirigasi`
  MODIFY `intIDJaringanIrigasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=330;

--
-- AUTO_INCREMENT for table `m_jenistanaman`
--
ALTER TABLE `m_jenistanaman`
  MODIFY `intIDJenisTanaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  MODIFY `intIDKecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3508201;

--
-- AUTO_INCREMENT for table `m_kewenangan_jaringan`
--
ALTER TABLE `m_kewenangan_jaringan`
  MODIFY `intIDKewenanganJaringan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_penyebabkerusakan`
--
ALTER TABLE `m_penyebabkerusakan`
  MODIFY `intIDPenyebabKerusakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_programintensifikasi`
--
ALTER TABLE `m_programintensifikasi`
  MODIFY `intIDProgramIntensifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_saluran`
--
ALTER TABLE `m_saluran`
  MODIFY `intIDSaluran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_stasiuncurahhujan`
--
ALTER TABLE `m_stasiuncurahhujan`
  MODIFY `intIDStasiunCurahHujan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_upt`
--
ALTER TABLE `m_upt`
  MODIFY `intIDUPT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `intIDuser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `m_user_admin`
--
ALTER TABLE `m_user_admin`
  MODIFY `intIDuser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_user_akses_jaringanirigasi`
--
ALTER TABLE `m_user_akses_jaringanirigasi`
  MODIFY `intIDAksesJaringanIrigasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_user_akses_stasiuncurahhujan`
--
ALTER TABLE `m_user_akses_stasiuncurahhujan`
  MODIFY `intIDAksesStasiunCurahHujan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_surveycurahhujan`
--
ALTER TABLE `tr_surveycurahhujan`
  MODIFY `intIDCurahHujan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tr_surveykerusakantanaman`
--
ALTER TABLE `tr_surveykerusakantanaman`
  MODIFY `intIDSurveyKerusakanTanaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tr_surveypintuair`
--
ALTER TABLE `tr_surveypintuair`
  MODIFY `intIDSurveyPintuAir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tr_surveyrealisasitanaman`
--
ALTER TABLE `tr_surveyrealisasitanaman`
  MODIFY `intIDSurveyRealisasiTanaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tr_survey_bangunan`
--
ALTER TABLE `tr_survey_bangunan`
  MODIFY `intIDSurveyBangunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tr_tanamanrusak`
--
ALTER TABLE `tr_tanamanrusak`
  MODIFY `intIDTanamanRusak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
