<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurahHujan extends Model {
    
    const ID_USER = 'in_intIDUser';
    const ID_CURAH_HUJAN = 'in_intIDCurahHujan';
    const ID_KECAMATAN = 'in_intIDKecamatan';
    const ID_STASIUN =  'in_intIDStasiunCurahHujan';
    const LIMIT = 'in_limit';
    const OFFSET = 'offset';
    const DBL_CURAH_HUJAN = 'in_dblCurahHujan';
    const DBL_LATITUDE = 'in_dblLatitude';
    const DBL_LONGITUDE = 'in_dblLongitude';
    const DT_SURVEY = 'in_dtSurvey';
}
