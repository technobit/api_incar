<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PintuAir extends Model {
    
    const ID_USER = 'in_intIDUser';
    const ID_JARINGAN_IRIGASI = 'in_intIDJaringanIrigasi';
    const ID_SALURAN = 'in_intIDSaluran';
    const ID_JENIS_SALURAN = 'in_intIDJenisSaluran';
    const ID_SURVEY = 'in_intIDSurveyPintuAir';
    const LIMIT = 'in_limit';
    const OFFSET = 'in_offset';
    const DBL_BAKU_SAWAH = 'in_dblBakuSawah';
    const PELA_DBL_DEBIT = 'in_Pelaksanaan_dblDebit';
    const PELA_DBL_LPR = 'in_Pelaksanaan_dblLPR';
    const PELA_DBL_FPR = 'in_Pelaksanaan_dblFPR';
    const PERA_DBL_DEBIT = 'in_Perencanaan_dblDebit';
    const PERA_DBL_LPR = 'in_Perencanaan_dblLPR';
    const PERA_DBL_FPR = 'in_Perencanaan_dblFPR';
    const DT_SURVEY = 'in_dtSurvey';
}
