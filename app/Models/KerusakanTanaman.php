<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class KerusakanTanaman extends Model {
    const ID_USER = 'in_intIDUser';
    const ID_DESA = 'in_intIDDesa';
    const ID_KECAMATAN = 'in_intIDKecamatan';
    const ID_JARINGAN_IRIGASI = 'in_intIDJaringanIrigasi';
    const ID_SALURAN = 'in_intIDSaluran';
    const ID_JENIS_SALURAN = 'in_intIDJenisSaluran';
    const ID_PROG_INSTENSIF = 'in_intIDProgramIntensifikasi';
    const ID_PENY_KERUSAKAN = 'in_intIDPenyebabKerusakan';
    const ID_SURVEY_KERUSAKAN = 'in_intIDSurveyKerusakanTanaman';
    const ID_JENIS_TANAMAN = 'in_intIDJenisTanaman';
    const LIMIT = 'in_limit';
    const OFFSET = 'in_offset';
    const DBL_BAKU_SAWAH = 'in_dblBakuSawah';
    const DBL_MATI = 'in_dblMati';
    const DBL_PUSO = 'in_dblPuso';
    const DBL_BERAT = 'in_dblBerat';
    const DBL_SEDANG = 'in_dblSedang';
    const DBL_RINGAN = 'in_dblRingan';
    const DBL_LAMA_GENANGAN = 'in_dblLamaGenangan';
    const KETERANGAN = 'in_txtKeterangan';
    const DT_SURVEY = 'in_dtSurvey';
}
