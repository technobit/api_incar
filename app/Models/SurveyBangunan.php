<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyBangunan extends Model {
    
    const ID_USER = 'in_intIDUser';
    const ID_JARINGAN_IRIGASI = 'in_intIDJaringanIrigasi';
    const ID_BANGUNAN = 'in_intIDBangunan';
    const ID_JENIS_SALURAN = 'in_intIDJenisSaluran';
    const ID_SURVEY = 'in_intIDSurveyBangunan';
    const LIMIT = 'in_limit';
    const OFFSET = 'in_offset';
    const DBL_LATITUDE = 'in_dblLatitude';
    const DBL_LONGITUDE = 'in_dblLongitude';
    const KONDISI = 'in_intKondisi';
    const FUNGSI = 'in_intFungsi';
    const PENGARUH_ALIRAN_AIR = 'in_intMempengaruhiAliranAir';
    const FOTO_1 = 'in_txtFoto1';
    const FOTO_2 = 'in_txtFoto2';
    const FOTO_3 = 'in_txtFoto3';
    const FOTO_4 = 'in_txtFoto4';
    const FOTO_5 = 'in_txtFoto5';
    const KETERANGAN = 'in_txtKeterangan';
    const DT_SURVEY = 'in_dtSurvey';
    
    const F_FOTO = 'in_txtFoto';

}
