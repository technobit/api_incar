<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RealisasiTanaman extends Model {
    
    const ID_USER = 'in_intIDUser';
    const ID_JARINGAN_IRIGASI = 'in_intIDJaringanIrigasi';
    const ID_SALURAN = 'in_intIDSaluran';
    const ID_JENIS_SALURAN = 'in_intIDJenisSaluran';
    const ID_SURVEY = 'in_intIDSurveyRealisasiTanaman';
    const LIMIT = 'in_limit';
    const OFFSET = 'in_offset';
    const DBL_BAKU_SAWAH = 'in_dblBakuSawah';
    const KETERANGAN = 'in_txtKeterangan';
    const DEBIT_INTAKE = 'in_Debit_Intake';
    const DEBIT_IJIN_AIR = 'in_Debit_IjinAir';
    const DEBIT_EFISIENSI = 'in_Debit_Efisiensi';
    const DEBIT_TOTAL_TERSIER = 'in_Debit_TotalTersier';
    const DBL_LPR = 'in_dblLPR';
    const DBL_FPR = 'in_dblFPR';
    const PADI_MH_BIBIT = 'in_Padi_MH_dblBibit';
    const PADI_MH_GARAP = 'in_Padi_MH_dblGarap';
    const PADI_MH_TANAM = 'in_Padi_MH_dblTanam';
    const PADI_MK1_IJIN_BIBIT = 'in_Padi_MK1_GaduIjin_dblBibit';
    const PADI_MK1_IJIN_GARAP = 'in_Padi_MK1_GaduIjin_dblGarap';
    const PADI_MK1_IJIN_TANAM = 'in_Padi_MK1_GaduIjin_dblTanam';
    const PADI_MK1_TIDAK_BIBIT = 'in_Padi_MK1_GaduTidakIjin_dblBibit';
    const PADI_MK1_TIDAK_GARAP = 'in_Padi_MK1_GaduTidakIjin_dblGarap';
    const PADI_MK1_TIDAK_TANAM = 'in_Padi_MK1_GaduTidakIjin_dblTanam';
    const PADI_MK2_IJIN_BIBIT = 'in_Padi_MK2_GaduIjin_dblBibit';
    const PADI_MK2_IJIN_GARAP = 'in_Padi_MK2_GaduIjin_dblGarap';
    const PADI_MK2_IJIN_TANAM = 'in_Padi_MK2_GaduIjin_dblTanam';
    const PADI_MK2_TIDAK_BIBIT = 'in_Padi_MK2_GaduTidakIjin_dblBibit';
    const PADI_MK2_TIDAK_GARAP = 'in_Padi_MK2_GaduTidakIjin_dblGarap';
    const PADI_MK2_TIDAK_TANAM = 'in_Padi_MK2_GaduTidakIjin_dblTanam';
    const PADI_TOTAL = 'in_Padi_Total';
    const TEBU_RYT_CEMPLONG = 'in_TebuRakyat_dblCemplong';
    const TEBU_RYT_BIBIT = 'in_TebuRakyat_dblBibit';
    const TEBU_RYT_MUDA = 'in_TebuRakyat_dblMuda';
    const TEBU_RYT_TUA = 'in_TebuRakyat_dblTua';
    const TEBU_RYT_TOTAL = 'in_TebuRakyat_dblTotal';
    const TEBU_PERU_CEMPLONG = 'in_TebuPerusahaan_dblCemplong';
    const TEBU_PERU_BIBIT = 'in_TebuPerusahaan_dblBibit';
    const TEBU_PERU_MUDA = 'in_TebuPerusahaan_dblMuda';
    const TEBU_PERU_TUA = 'in_TebuPerusahaan_dblTua';
    const TEBU_PERU_TOTAL = 'in_TebuPerusahaan_dblTotal';
    const POLO_MH = 'in_Polowijo_MH';
    const POLO_MK1_JAGUNG = 'in_Polowijo_MK1_Jagung';
    const POLO_MK1_KEDELAI = 'in_Polowijo_MK1_Kedelai';
    const POLO_MK1_LAIN = 'in_Polowijo_MK1_Lainnya';
    const POLO_MK2_JAGUNG = 'in_Polowijo_MK2_Jagung';
    const POLO_MK2_KEDELAI = 'in_Polowijo_MK2_Kedelai';
    const POLO_MK2_LAIN = 'in_Polowijo_MK2_Lainnya';
    const POLO_TEMBAKAU = 'in_Polowijo_Tembakau';
    const POLO_ROSELA = 'in_Polowijo_Rosela';
    const POLO_TOTAL = 'in_Polowijo_Total';
    const BERO_ASLI = 'in_Bero_dblAsli';
    const BERO_PASCAPANEN = 'in_Bero_dblPascaPanen';
    const BERO_TOTAL = 'in_Bero_Total';
    const DT_SURVEY = 'in_dtSurvey';

}
