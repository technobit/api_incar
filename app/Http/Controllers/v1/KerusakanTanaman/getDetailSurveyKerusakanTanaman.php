<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\KerusakanTanaman;

class getDetailSurveyKerusakanTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::ID_SURVEY_KERUSAKAN   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $data = DB::select('call mobile_getDetailSurveyKerusakanTanaman(?)', [
            $request[KerusakanTanaman::ID_SURVEY_KERUSAKAN]
        ]);

        return APIresponse(true, 'Data Survey Kerusakan Tanaman Berhasil Ditemukan', $data);
    }

}
