<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getProgramIntensifikasi extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getProgramIntensifikasi');

        return APIresponse(true, 'Data Program Intensifikasi Berhasil Ditemukan!', $data);
    }
}
