<?php

namespace App\Http\Controllers\v1\KerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\KerusakanTanaman;

class getRowCountDataSurveyKerusakanTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getRowCountDataSurveyKerusakanTanaman(?)', [
            $request[KerusakanTanaman::ID_USER]
        ]);
        
        return APIresponse(true, 'Data Banyak Survey Kerusakan Tanaman Berhasil!', $data);
    }
}
