<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\KerusakanTanaman;

class getAllHistorySurveyKerusakanTanaman extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getAllHistorySurveyKerusakanTanaman(?)', [
            $request[KerusakanTanaman::ID_USER]
        ]); 

        foreach ($data as $key => $value) {
            $tanamanRusak = DB::select('call mobile_getDetailSurveyJenisTanamanRusak(?)', [
                $value->intIDSurveyKerusakanTanaman
            ]); 
            $data[$key]->tanamanRusak = $tanamanRusak;
        };

        return APIresponse(true, 'Data Seluruh Survey Jenis Tanaman Rusak Berhasil Ditemukan!', $data);
    }
}
