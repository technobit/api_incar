<?php

namespace App\Http\Controllers\v1\KerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\KerusakanTanaman;

class getHistorySurveyKerusakanTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::LIMIT     => 'required',
            KerusakanTanaman::OFFSET    => 'required',
            KerusakanTanaman::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        
        $data = DB::select('call mobile_getHistorySurveyKerusakanTanaman(?,?,?)', [
            $request[KerusakanTanaman::LIMIT],
            $request[KerusakanTanaman::OFFSET],
            $request[KerusakanTanaman::ID_USER]
        ]);

        return APIresponse(true, 'Data Survey Kerusakan Tanaman Berhasil Ditemukan', $data);
    }
}
