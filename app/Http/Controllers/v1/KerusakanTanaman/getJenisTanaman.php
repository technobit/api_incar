<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getJenisTanaman extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getJenisTanaman');

        return APIresponse(true, 'Data Jenis Tanaman Berhasil Ditemukan!', $data);
    }
}
