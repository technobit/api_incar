<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getPenyebabKerusakan extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getPenyebabKerusakan');

        return APIresponse(true, 'Data Penyebab Kerusakan Berhasil Ditemukan!', $data);
    }
}
