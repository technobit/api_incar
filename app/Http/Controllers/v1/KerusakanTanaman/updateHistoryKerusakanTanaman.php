<?php

namespace App\Http\Controllers\v1\kerusakanTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\KerusakanTanaman;

class updateHistoryKerusakanTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::ID_SURVEY_KERUSAKAN   => 'required',
            KerusakanTanaman::ID_JARINGAN_IRIGASI   => 'required',
            KerusakanTanaman::ID_SALURAN            => 'required',
            KerusakanTanaman::DBL_BAKU_SAWAH        => 'required',
            KerusakanTanaman::ID_KECAMATAN          => 'required',
            KerusakanTanaman::ID_DESA               => 'required',
            KerusakanTanaman::ID_PROG_INSTENSIF     => 'required',
            KerusakanTanaman::ID_PENY_KERUSAKAN     => 'required',
            // KerusakanTanaman::DBL_MATI              => 'required',
            // KerusakanTanaman::DBL_PUSO              => 'required',
            // KerusakanTanaman::DBL_BERAT             => 'required',
            // KerusakanTanaman::DBL_SEDANG            => 'required',
            // KerusakanTanaman::DBL_RINGAN            => 'required',
            // KerusakanTanaman::DBL_LAMA_GENANGAN     => 'required',
            // KerusakanTanaman::KETERANGAN            => 'required',
            KerusakanTanaman::DT_SURVEY             => 'required',
            // KerusakanTanaman::ID_JENIS_TANAMAN      => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        DB::select('call mobile_updateHistoryKerusakanTanaman(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request[KerusakanTanaman::ID_SURVEY_KERUSAKAN],
            $request[KerusakanTanaman::ID_JARINGAN_IRIGASI],
            $request[KerusakanTanaman::ID_SALURAN],
            $request[KerusakanTanaman::DBL_BAKU_SAWAH],
            $request[KerusakanTanaman::ID_KECAMATAN],
            $request[KerusakanTanaman::ID_DESA],
            $request[KerusakanTanaman::ID_PROG_INSTENSIF],
            $request[KerusakanTanaman::ID_PENY_KERUSAKAN],
            $request[KerusakanTanaman::DBL_MATI] ?? null ,
            $request[KerusakanTanaman::DBL_PUSO] ?? null ,
            $request[KerusakanTanaman::DBL_BERAT] ?? null ,
            $request[KerusakanTanaman::DBL_SEDANG] ?? null ,
            $request[KerusakanTanaman::DBL_RINGAN] ?? null ,
            $request[KerusakanTanaman::DBL_LAMA_GENANGAN] ?? null ,
            $request[KerusakanTanaman::KETERANGAN] ?? null ,
            $request[KerusakanTanaman::DT_SURVEY],
        ]); 

        if (!empty($request[KerusakanTanaman::ID_JENIS_TANAMAN])) {
            foreach ($request[KerusakanTanaman::ID_JENIS_TANAMAN] as $key => $value) {
                DB::select('call mobile_updateHistoryJenisTanamanRusak(?,?,?)', [
                    $request[KerusakanTanaman::ID_SURVEY_KERUSAKAN],
                    $value,
                    $request['in_isChecked'][$key] ? 1 : 0
                ]);
            }
        }

        return APIresponse(true, 'Data Survey Kerusakan Tanaman Berhasil Diperbarui!', null);
    }
}
