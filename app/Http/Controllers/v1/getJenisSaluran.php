<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getJenisSaluran extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getJenisSaluran');

        return APIresponse(true, 'Data Jenis Saluran Berhasil Ditemukan!', $data);
    }
}
