<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getAllSaluran extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getAllSaluran');

        return APIresponse(true, 'Data Seluruh Saluran Berhasil Ditemukan!', $data);
    }
}
