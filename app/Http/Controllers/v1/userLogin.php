<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class userLogin extends Controller {
    
    const USERNAME = 'in_txtUsername';
    const PASSWORD = 'in_txtPassword';

    /*function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SELF::USERNAME      => 'required',
            SELF::PASSWORD      => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        // hashing encrypt static
        $method = 'AES-128-CBC';
        $key = 'key_incar';
        $iv = 'Incar_Password20';

        $data = DB::select('call mobile_getUserAccount(?)', [
            $request[SELF::USERNAME],
            // openssl_encrypt($request[SELF::PASSWORD], $method, $key, 0, $iv)
        ]);

        if (!$data[0]->bitSuccess || !verifyPassword($request[SELF::PASSWORD], $data[0]->txtPassword)) 
            return APIresponse(false, 'Username Atau Password Tidak Ditemukan!', null);

        unset($data[0]->txtPassword);
        return APIresponse(true, 'Berhasil Login!', $data[0]);
    }*/
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SELF::USERNAME      => 'required',
            SELF::PASSWORD      => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        // hashing encrypt static
        $method = 'AES-128-CBC';
        $key = 'key_incar';
        $iv = 'Incar_Password20';

        $data = DB::select('call mobile_getUserAccount(?)', [
            $request[SELF::USERNAME],
            // openssl_encrypt($request[SELF::PASSWORD], $method, $key, 0, $iv)
        ]);

		if(password_verify($request[SELF::PASSWORD], $data[0]->txtPassword)){
			return APIresponse(true, 'Berhasil Login!', $data[0]);
		} else {
            return APIresponse(false, 'Username Atau Password Tidak Ditemukan!', null);
		}
		
        /*if (!$data[0]->bitSuccess || !verifyPassword($request[SELF::PASSWORD], $data[0]->txtPassword)) 
            return APIresponse(false, 'Username Atau Password Tidak Ditemukan!', null);

        unset($data[0]->txtPassword);
        return APIresponse(true, 'Berhasil Login!', $data[0]);*/
    }

	function password_verify($password, $hash)
	{
		if (strlen($hash) !== 60 OR strlen($password = crypt($password, $hash)) !== 60)
		{
			return FALSE;
		}

		$compare = 0;
		for ($i = 0; $i < 60; $i++)
		{
			$compare |= (ord($password[$i]) ^ ord($hash[$i]));
		}

		return ($compare === 0);
	}
}
