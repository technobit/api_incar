<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\models\PintuAir;
use Illuminate\Support\Facades\DB;

class getDataJaringanIrigasi extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getDataJaringanIrigasi(?)', [
            $request[PintuAir::ID_USER]
        ]);

        return APIresponse(true, 'Data Jaringan Berhasil Ditemukan!', $data);
    }
}