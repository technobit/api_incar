<?php

namespace App\Http\Controllers\v1\SurveyBangunan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SurveyBangunan;
use Illuminate\Support\Carbon;

class surveybangunan_insert extends Controller {
    
    const imagePath = 'images/bangunan';

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SurveyBangunan::ID_JARINGAN_IRIGASI     => 'required',
            SurveyBangunan::ID_BANGUNAN             => 'required',
            SurveyBangunan::ID_JENIS_SALURAN        => 'required',
            SurveyBangunan::DBL_LATITUDE            => 'required',
            SurveyBangunan::DBL_LONGITUDE           => 'required',
            // SurveyBangunan::KONDISI                 => 'required',
            // SurveyBangunan::FUNGSI                  => 'required',
            // SurveyBangunan::PENGARUH_ALIRAN_AIR     => 'required',
            // SurveyBangunan::FOTO_1                  => 'image|mimes:jpeg,png,jpg|max:4096',
            // SurveyBangunan::FOTO_2                  => 'image|mimes:jpeg,png,jpg|max:4096',
            // SurveyBangunan::FOTO_3                  => 'image|mimes:jpeg,png,jpg|max:4096',
            // SurveyBangunan::FOTO_4                  => 'image|mimes:jpeg,png,jpg|max:4096',
            // SurveyBangunan::FOTO_5                  => 'image|mimes:jpeg,png,jpg|max:4096',
            // SurveyBangunan::KETERANGAN              => 'required',
            SurveyBangunan::DT_SURVEY               => 'required',
            SurveyBangunan::ID_USER                 => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        // $credentials = [];
        // $now = Carbon::now();

        // if ($request->hasfile(SurveyBangunan::F_FOTO)) { 
        //     $index = 1;
        //     foreach ($request->file(SurveyBangunan::F_FOTO) as $image) {
        //         // $files = $value;
        //         // $index = $key+1;
        //         $destinationPath = public_path(SELF::imagePath);
        //         $filename = $request->in_intIDUser.'_bangunan_'.$index.'_'.$now->format('Y-m-d_H-i-s').'.'.$image->getClientOriginalExtension();
        //         $image->move($destinationPath, $filename);
        //         $credentials[SurveyBangunan::F_FOTO.$index] = $filename;
        //         $index++;
        //     }
        // }

        $request = $request->toArray();
        // unset($request[SurveyBangunan::F_FOTO]);

        // foreach ($credentials as $key => $value) {
        //     $request[$key] = $value;
        // }

        DB::select('call mobile_surveybangunan_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request[SurveyBangunan::ID_JARINGAN_IRIGASI],
            $request[SurveyBangunan::ID_BANGUNAN],
            $request[SurveyBangunan::ID_JENIS_SALURAN],
            $request[SurveyBangunan::DBL_LATITUDE],
            $request[SurveyBangunan::DBL_LONGITUDE],
            $request[SurveyBangunan::KONDISI] ?? null,
            $request[SurveyBangunan::FUNGSI] ?? null,
            $request[SurveyBangunan::PENGARUH_ALIRAN_AIR] ?? null,
            $request[SurveyBangunan::FOTO_1] ?? null,
            $request[SurveyBangunan::FOTO_2] ?? null,
            $request[SurveyBangunan::FOTO_3] ?? null,
            $request[SurveyBangunan::FOTO_4] ?? null,
            $request[SurveyBangunan::FOTO_5] ?? null,
            $request[SurveyBangunan::KETERANGAN] ?? null,
            $request[SurveyBangunan::DT_SURVEY],
            $request[SurveyBangunan::ID_USER]
        ]);

        return APIresponse(true, 'Data Survey Bangunan Berhasil Disimpan!', null);
    }
}
