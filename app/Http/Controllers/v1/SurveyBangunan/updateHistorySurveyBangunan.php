<?php

namespace App\Http\Controllers\v1\SurveyBangunan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SurveyBangunan;

class updateHistorySurveyBangunan extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SurveyBangunan::ID_SURVEY               => 'required',
            SurveyBangunan::ID_JARINGAN_IRIGASI     => 'required',
            SurveyBangunan::ID_BANGUNAN             => 'required',
            SurveyBangunan::ID_JENIS_SALURAN        => 'required',
            SurveyBangunan::KONDISI                 => 'required',
            SurveyBangunan::FUNGSI                  => 'required',
            SurveyBangunan::PENGARUH_ALIRAN_AIR     => 'required',
            // SurveyBangunan::FOTO_1                  => 'required',
            // SurveyBangunan::FOTO_2                  => 'required',
            // SurveyBangunan::FOTO_3                  => 'required',
            // SurveyBangunan::FOTO_4                  => 'required',
            // SurveyBangunan::FOTO_5                  => 'required',
            // SurveyBangunan::KETERANGAN              => 'required',
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_updateHistorySurveyBangunan(?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request[SurveyBangunan::ID_SURVEY],
            $request[SurveyBangunan::ID_JARINGAN_IRIGASI],
            $request[SurveyBangunan::ID_BANGUNAN],
            $request[SurveyBangunan::ID_JENIS_SALURAN],
            $request[SurveyBangunan::KONDISI],
            $request[SurveyBangunan::FUNGSI],
            $request[SurveyBangunan::PENGARUH_ALIRAN_AIR],
            $request[SurveyBangunan::FOTO_1] ?? null,
            $request[SurveyBangunan::FOTO_2] ?? null,
            $request[SurveyBangunan::FOTO_3] ?? null,
            $request[SurveyBangunan::FOTO_4] ?? null,
            $request[SurveyBangunan::FOTO_5] ?? null,
            $request[SurveyBangunan::KETERANGAN] ?? null
        ]);

        return APIresponse(true, 'Data Survey Bangunan Berhasil Diperbarui!', null);
    }
}
