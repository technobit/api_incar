<?php

namespace App\Http\Controllers\v1\SurveyBangunan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SurveyBangunan;
use Illuminate\Support\Carbon;

class uploadPhotoSurveyBangunan extends Controller {

    const IMAGE_PATH = '../../e-incar-cdn-survey/bangunan';
    const NAME_PREFIX = '_bangunan_';
    
    function __invoke(Request $request) {
        $now = Carbon::now();
        $filename = '';

        if ($request->hasfile(SurveyBangunan::F_FOTO)) { 
            // $index = 1;
            $image = $request->file(SurveyBangunan::F_FOTO);
            $destinationPath = public_path(SELF::IMAGE_PATH);
            $filename = $now->format('Y-m-d_H-i-s').SELF::NAME_PREFIX.mt_rand(1000000, 9999999).'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $filename);
            // $credentials[SurveyBangunan::F_FOTO] = $filename;

            // foreach ($request->file(SurveyBangunan::F_FOTO) as $image) {
            //     // $index = $key+1;
            //     $destinationPath = public_path(SELF::imagePath);
            //     $filename = $request->in_intIDUser.'_bangunan_'.$index.'_'.$now->format('Y-m-d_H-i-s').'.'.$image->getClientOriginalExtension();
            //     $image->move($destinationPath, $filename);
            //     $credentials[SurveyBangunan::F_FOTO.$index] = $filename;
            //     $index++;
            // }
        }

        $filename = 'http://cdn-survey.e-incar.com/bangunan/'.$filename;

        return APIresponse(true, 'Upload Photo Survey Bangunan Berhasil!', $filename);
    }
}
