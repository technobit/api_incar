<?php

namespace App\Http\Controllers\v1\SurveyBangunan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getDataBangunan extends Controller {
    
    function __invoke() {
        $data = DB::select('call mobile_getDataBangunan');

        return APIresponse(true, 'Data Bangunan Berhasil Ditemukan!', $data);
    }
}
