<?php

namespace App\Http\Controllers\v1\SurveyBangunan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SurveyBangunan;

class getRowCountDataSurveyBangunan extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            SurveyBangunan::ID_USER     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getRowCountDataSurveyBangunan(?)', [
            $request[SurveyBangunan::ID_USER]
        ]);

        return APIresponse(true, 'Data Banyak Survey Bangunan Berhasil!', $data);
    }
}
