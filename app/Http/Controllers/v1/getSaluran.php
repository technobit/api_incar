<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\PintuAir;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class getSaluran extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::ID_JARINGAN_IRIGASI       => 'required',
            PintuAir::ID_JENIS_SALURAN          => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data =DB::select('call mobile_getSaluran(?,?)', [
            $request[PintuAir::ID_JARINGAN_IRIGASI],
            $request[PintuAir::ID_JENIS_SALURAN]
        ]);

        return APIresponse(true, 'Data Saluran Berhasil Ditemukan', $data);
    }
}
