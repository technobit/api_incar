<?php

namespace App\Http\Controllers\v1\curahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
USE App\Models\CurahHujan;

class getHistorySurveyCurahHujan extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            CURAHHUJAN::LIMIT     => 'required',
            CURAHHUJAN::OFFSET    => 'required',
            CURAHHUJAN::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getHistorySurveyCurahHujan(?,?,?)', [
            $request[CURAHHUJAN::LIMIT], $request[CURAHHUJAN::OFFSET], $request[CURAHHUJAN::ID_USER]
        ]);

        return APIresponse(true, 'Data History Survey Curah Hujan Berhasil!', $data);
    }
}
