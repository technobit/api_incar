<?php

namespace App\Http\Controllers\v1\curahHujan;

use App\Http\Controllers\Controller;
use App\Models\CurahHujan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class getAllHistorySurveyCurahHujan extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            CURAHHUJAN::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $data = DB::select('call mobile_getAllHistorySurveyCurahHujan(?)', [
            $request[CurahHujan::ID_USER]
        ]);

        return APIresponse(true, 'Data Seluruh Survey Curah Hujan Berhasil Ditemukan!', $data);
    }
}
