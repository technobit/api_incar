<?php

namespace App\Http\Controllers\v1\curahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\CurahHujan;

class delHistorySurveyCurahHujan extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            curahHujan::ID_CURAH_HUJAN      => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        DB::select('call mobile_delHistorySurveyCurahHujan(?)', [
            $request[curahHujan::ID_CURAH_HUJAN]
        ]);

        return APIresponse(true, 'Histori Survey Curah Hujan Berhasil Dihapus!', null);

    }

}
