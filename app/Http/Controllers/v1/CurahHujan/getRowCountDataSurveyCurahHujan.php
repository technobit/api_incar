<?php

namespace App\Http\Controllers\v1\curahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\CurahHujan;

class getRowCountDataSurveyCurahHujan extends Controller{

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(),[
            CURAHHUJAN::ID_USER   => 'required|integer'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getRowCountDataSurveyCurahHujan(?)', [
            $request[CURAHHUJAN::ID_USER]
        ]);
        
        return APIresponse(true, 'Data Banyak Survey Curah Hujan Berhasil!', $data);
    }
}
