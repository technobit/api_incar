<?php

namespace App\Http\Controllers\v1\CurahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\CurahHujan;

class updateHistorySurveyCurahHujan extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            CurahHujan::ID_CURAH_HUJAN      => 'required',
            CurahHujan::DBL_CURAH_HUJAN     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_updateHistorySurveyCurahHujan(?,?)', [
            $request[CurahHujan::ID_CURAH_HUJAN],
            $request[CurahHujan::DBL_CURAH_HUJAN]
        ]);

        return APIresponse(true, 'Update History Survey Curah Hujan Berhasil!', null);
    }
}
