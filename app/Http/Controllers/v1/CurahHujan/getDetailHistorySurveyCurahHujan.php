<?php

namespace App\Http\Controllers\v1\CurahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\CurahHujan;

class getDetailHistorySurveyCurahHujan extends Controller {

    public function __invoke(Request $request) {
        // return response(['data' => $request]);
        $validator = Validator::make($request->all(),[
            CURAHHUJAN::ID_CURAH_HUJAN => 'required|integer'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getDetailHistorySurveyCurahHujan(?)',[
            $request[CURAHHUJAN::ID_CURAH_HUJAN]
        ]);

        return APIresponse(true, 'Detail Histori Survey Curah Hujan Berhasil!', $data);
    }

}
