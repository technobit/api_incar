<?php

namespace App\Http\Controllers\v1\CurahHujan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\CurahHujan;

class surveyCurahHujan_insert extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            CurahHujan::ID_KECAMATAN        => 'required',
            CurahHujan::ID_STASIUN          => 'required',
            CurahHujan::DBL_CURAH_HUJAN     => 'required',
            CurahHujan::DBL_LATITUDE        => 'required',
            CurahHujan::DBL_LONGITUDE       => 'required',
            CurahHujan::DT_SURVEY           => 'required',
            CurahHujan::ID_USER             => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        DB::select('call mobile_surveyCurahHujan_insert(?,?,?,?,?,?,?)', [
            $request[CurahHujan::ID_KECAMATAN],
            $request[CurahHujan::ID_STASIUN],
            $request[CurahHujan::DBL_CURAH_HUJAN],
            $request[CurahHujan::DBL_LATITUDE],
            $request[CurahHujan::DBL_LONGITUDE],
            $request[CurahHujan::DT_SURVEY],
            $request[CurahHujan::ID_USER]          
        ]);

        return APIresponse(true, 'Survey Curah Hujan Berhasil Tersimpan!', null);
    }
}
