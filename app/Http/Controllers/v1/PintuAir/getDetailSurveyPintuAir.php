<?php

namespace App\Http\Controllers\v1\pintuAir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\PintuAir;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class getDetailSurveyPintuAir extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::ID_SURVEY     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getDetailSurveyPintuAir(?)', [
            $request[PintuAir::ID_SURVEY]
        ]);

        return APIresponse(true, 'Data Survey Pintu Air Berhasil Ditemukan!', $data);
    }
}
