<?php

namespace App\Http\Controllers\v1\pintuAir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\PintuAir;

class updateHistorySurveyPintuAir extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::ID_SURVEY             => 'required',
            PintuAir::ID_JARINGAN_IRIGASI   => 'required',
            PintuAir::ID_SALURAN            => 'required',
            PintuAir::DBL_BAKU_SAWAH        => 'required',
            // PintuAir::PELA_DBL_DEBIT        => 'required',
            // PintuAir::PELA_DBL_LPR          => 'required',
            // PintuAir::PELA_DBL_FPR          => 'required',
            // PintuAir::PERA_DBL_DEBIT        => 'required',
            // PintuAir::PERA_DBL_LPR          => 'required',
            // PintuAir::PERA_DBL_FPR          => 'required',
            PintuAir::DT_SURVEY             => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_updateHistorySurveyPintuAir(?,?,?,?,?,?,?,?,?,?,?)', [
            $request[PintuAir::ID_SURVEY],
            $request[PintuAir::ID_JARINGAN_IRIGASI],
            $request[PintuAir::ID_SALURAN],
            $request[PintuAir::DBL_BAKU_SAWAH],
            $request[PintuAir::PELA_DBL_DEBIT] ?? null ,
            $request[PintuAir::PELA_DBL_LPR] ?? null ,
            $request[PintuAir::PELA_DBL_FPR] ?? null ,
            $request[PintuAir::PERA_DBL_DEBIT] ?? null ,
            $request[PintuAir::PERA_DBL_LPR] ?? null ,
            $request[PintuAir::PERA_DBL_FPR] ?? null ,
            $request[PintuAir::DT_SURVEY]
        ]);
        
        return APIresponse(true, 'Data Survey Pintu Air Berhasil Diperbarui!', null);
    }
}
