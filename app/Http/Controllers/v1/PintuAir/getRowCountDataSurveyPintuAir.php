<?php

namespace App\Http\Controllers\v1\pintuAir;

use App\Http\Controllers\Controller;
use App\models\PintuAir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class getRowCountDataSurveyPintuAir extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::ID_USER       => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getRowCountDataSurveyPintuAir(?)', [
            $request[PintuAir::ID_USER]
        ]);

        return APIresponse(true, 'Data Banyak Survey Pintu Air Behasil!', $data);
    }
}
