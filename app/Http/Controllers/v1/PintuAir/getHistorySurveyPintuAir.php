<?php

namespace App\Http\Controllers\v1\pintuAir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\PintuAir;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class getHistorySurveyPintuAir extends Controller {

    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            PintuAir::LIMIT     => 'required',
            PintuAir::OFFSET    => 'required',
            PintuAir::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getHistorySurveyPintuAir(?,?,?)', [
            $request[PintuAir::LIMIT],
            $request[PintuAir::OFFSET],
            $request[PintuAir::ID_USER]
        ]);

        return APIresponse(true, 'Data Survey Pintu Air Berhasil Ditemukan!', $data);
    }
}
