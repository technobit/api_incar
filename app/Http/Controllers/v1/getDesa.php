<?php

namespace App\Http\Controllers\v1;

// use App\Helpers\APIHelp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\models\KerusakanTanaman;

class getDesa extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            KerusakanTanaman::ID_KECAMATAN  => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(true, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        $data = DB::select('call getDesa(?)', [
            $request[KerusakanTanaman::ID_KECAMATAN]
        ]);

        return APIresponse(true, 'Data Desa Berhasil Ditemukan!', $data);
        
    }
}
