<?php

namespace App\Http\Controllers\v1\realisasiTanaman;

use App\Http\Controllers\Controller;
use App\Models\RealisasiTanaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class delHistorySurveyRealisasiTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            RealisasiTanaman::ID_SURVEY     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();
        DB::select('call mobile_delHistorySurveyRealisasiTanaman(?)', [
            $request[RealisasiTanaman::ID_SURVEY]
        ]);

        return APIresponse(true, 'Histori Survey Realisasi Tanaman Berhasil Dihapus!', null);
    }
}
