<?php

namespace App\Http\Controllers\v1\RealisasiTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RealisasiTanaman;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class getRowCountDataSurveyRealisasiTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            RealisasiTanaman::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getRowCountDataSurveyRealisasiTanaman(?)', [
            $request[RealisasiTanaman::ID_USER]
        ]);

        return APIresponse(true, 'Data Banyak Survey Realisasi Tanaman Berhasil!', $data);
    }
}
