<?php

namespace App\Http\Controllers\v1\RealisasiTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\RealisasiTanaman;

class surveyrealisasitanaman_insert extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            RealisasiTanaman::ID_JARINGAN_IRIGASI   => 'required',
            RealisasiTanaman::ID_SALURAN            => 'required',
            RealisasiTanaman::DBL_BAKU_SAWAH        => 'required',
        //     RealisasiTanaman::KETERANGAN            => 'required',
        //     RealisasiTanaman::DEBIT_INTAKE          => 'required',
        //     RealisasiTanaman::DEBIT_IJIN_AIR        => 'required',
        //     RealisasiTanaman::DEBIT_EFISIENSI       => 'required',
        //     RealisasiTanaman::DEBIT_TOTAL_TERSIER   => 'required',
        //     RealisasiTanaman::DBL_LPR               => 'required',
        //     RealisasiTanaman::DBL_FPR               => 'required',
        //     RealisasiTanaman::PADI_MH_BIBIT         => 'required',
        //     RealisasiTanaman::PADI_MH_GARAP         => 'required',
        //     RealisasiTanaman::PADI_MH_TANAM         => 'required',
        //     RealisasiTanaman::PADI_MK1_IJIN_BIBIT   => 'required',
        //     RealisasiTanaman::PADI_MK1_IJIN_GARAP   => 'required',
        //     RealisasiTanaman::PADI_MK1_IJIN_TANAM   => 'required',
        //     RealisasiTanaman::PADI_MK1_TIDAK_BIBIT  => 'required',
        //     RealisasiTanaman::PADI_MK1_TIDAK_GARAP  => 'required',
        //     RealisasiTanaman::PADI_MK1_TIDAK_TANAM  => 'required',
        //     RealisasiTanaman::PADI_MK2_IJIN_BIBIT   => 'required',
        //     RealisasiTanaman::PADI_MK2_IJIN_GARAP   => 'required',
        //     RealisasiTanaman::PADI_MK2_IJIN_TANAM   => 'required',
        //     RealisasiTanaman::PADI_MK2_TIDAK_BIBIT  => 'required',
        //     RealisasiTanaman::PADI_MK2_TIDAK_GARAP  => 'required',
        //     RealisasiTanaman::PADI_MK2_TIDAK_TANAM  => 'required',
        //     RealisasiTanaman::PADI_TOTAL            => 'required',
        //     RealisasiTanaman::TEBU_RYT_CEMPLONG     => 'required',
        //     RealisasiTanaman::TEBU_RYT_BIBIT        => 'required',
        //     RealisasiTanaman::TEBU_RYT_MUDA         => 'required',
        //     RealisasiTanaman::TEBU_RYT_TUA          => 'required',
        //     RealisasiTanaman::TEBU_RYT_TOTAL        => 'required',
        //     RealisasiTanaman::TEBU_PERU_CEMPLONG    => 'required',
        //     RealisasiTanaman::TEBU_PERU_BIBIT       => 'required',
        //     RealisasiTanaman::TEBU_PERU_MUDA        => 'required',
        //     RealisasiTanaman::TEBU_PERU_TUA         => 'required',
        //     RealisasiTanaman::TEBU_PERU_TOTAL       => 'required',
        //     RealisasiTanaman::POLO_MH               => 'required',
        //     RealisasiTanaman::POLO_MK1_JAGUNG       => 'required',
        //     RealisasiTanaman::POLO_MK1_KEDELAI      => 'required',
        //     RealisasiTanaman::POLO_MK1_LAIN         => 'required',
        //     RealisasiTanaman::POLO_MK2_JAGUNG       => 'required',
        //     RealisasiTanaman::POLO_MK2_KEDELAI      => 'required',
        //     RealisasiTanaman::POLO_MK2_LAIN         => 'required',
        //     RealisasiTanaman::POLO_TEMBAKAU         => 'required',
        //     RealisasiTanaman::POLO_ROSELA           => 'required',
        //     RealisasiTanaman::POLO_TOTAL            => 'required',
        //     RealisasiTanaman::BERO_ASLI             => 'required',
        //     RealisasiTanaman::BERO_PASCAPANEN       => 'required',
        //     RealisasiTanaman::BERO_TOTAL            => 'required',
            RealisasiTanaman::DT_SURVEY             => 'required',
            RealisasiTanaman::ID_USER               => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        DB::select('call mobile_surveyrealisasitanaman_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request[RealisasiTanaman::ID_JARINGAN_IRIGASI] ?? null,
            $request[RealisasiTanaman::ID_SALURAN] ?? null,
            $request[RealisasiTanaman::DBL_BAKU_SAWAH] ?? null,
            $request[RealisasiTanaman::KETERANGAN] ?? null,
            $request[RealisasiTanaman::DEBIT_INTAKE] ?? null,
            $request[RealisasiTanaman::DEBIT_IJIN_AIR] ?? null,
            $request[RealisasiTanaman::DEBIT_EFISIENSI] ?? null,
            $request[RealisasiTanaman::DEBIT_TOTAL_TERSIER] ?? null,
            $request[RealisasiTanaman::DBL_LPR] ?? null,
            $request[RealisasiTanaman::DBL_FPR] ?? null,
            $request[RealisasiTanaman::PADI_MH_BIBIT] ?? null,
            $request[RealisasiTanaman::PADI_MH_GARAP] ?? null,
            $request[RealisasiTanaman::PADI_MH_TANAM] ?? null,
            $request[RealisasiTanaman::PADI_MK1_IJIN_BIBIT] ?? null,
            $request[RealisasiTanaman::PADI_MK1_IJIN_GARAP] ?? null,
            $request[RealisasiTanaman::PADI_MK1_IJIN_TANAM] ?? null,
            $request[RealisasiTanaman::PADI_MK1_TIDAK_BIBIT] ?? null,
            $request[RealisasiTanaman::PADI_MK1_TIDAK_GARAP] ?? null,
            $request[RealisasiTanaman::PADI_MK1_TIDAK_TANAM] ?? null,
            $request[RealisasiTanaman::PADI_MK2_IJIN_BIBIT] ?? null,
            $request[RealisasiTanaman::PADI_MK2_IJIN_GARAP] ?? null,
            $request[RealisasiTanaman::PADI_MK2_IJIN_TANAM] ?? null,
            $request[RealisasiTanaman::PADI_MK2_TIDAK_BIBIT] ?? null,
            $request[RealisasiTanaman::PADI_MK2_TIDAK_GARAP] ?? null,
            $request[RealisasiTanaman::PADI_MK2_TIDAK_TANAM] ?? null,
            $request[RealisasiTanaman::PADI_TOTAL] ?? null,
            $request[RealisasiTanaman::TEBU_RYT_CEMPLONG] ?? null,
            $request[RealisasiTanaman::TEBU_RYT_BIBIT] ?? null,
            $request[RealisasiTanaman::TEBU_RYT_MUDA] ?? null,
            $request[RealisasiTanaman::TEBU_RYT_TUA] ?? null,
            $request[RealisasiTanaman::TEBU_RYT_TOTAL] ?? null,
            $request[RealisasiTanaman::TEBU_PERU_CEMPLONG] ?? null,
            $request[RealisasiTanaman::TEBU_PERU_BIBIT] ?? null,
            $request[RealisasiTanaman::TEBU_PERU_MUDA] ?? null,
            $request[RealisasiTanaman::TEBU_PERU_TUA] ?? null,
            $request[RealisasiTanaman::TEBU_PERU_TOTAL] ?? null,
            $request[RealisasiTanaman::POLO_MH] ?? null,
            $request[RealisasiTanaman::POLO_MK1_JAGUNG] ?? null,
            $request[RealisasiTanaman::POLO_MK1_KEDELAI] ?? null,
            $request[RealisasiTanaman::POLO_MK1_LAIN] ?? null,
            $request[RealisasiTanaman::POLO_MK2_JAGUNG] ?? null,
            $request[RealisasiTanaman::POLO_MK2_KEDELAI] ?? null,
            $request[RealisasiTanaman::POLO_MK2_LAIN] ?? null,
            $request[RealisasiTanaman::POLO_TEMBAKAU] ?? null,
            $request[RealisasiTanaman::POLO_ROSELA] ?? null,
            $request[RealisasiTanaman::POLO_TOTAL] ?? null,
            $request[RealisasiTanaman::BERO_ASLI] ?? null,
            $request[RealisasiTanaman::BERO_PASCAPANEN] ?? null,
            $request[RealisasiTanaman::BERO_TOTAL] ?? null,
            $request[RealisasiTanaman::DT_SURVEY] ?? null,
            $request[RealisasiTanaman::ID_USER] ?? null
        ]);

        return APIresponse(true, 'Data Survey Realisasi Tanaman Berhasil Disimpan!', null);
    }
}
