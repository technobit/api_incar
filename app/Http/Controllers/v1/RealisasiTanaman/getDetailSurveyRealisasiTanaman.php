<?php

namespace App\Http\Controllers\v1\realisasiTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\RealisasiTanaman;

class getDetailSurveyRealisasiTanaman extends Controller {
    
    function __invoke(Request $request) {
        $validator = Validator::make($request->all(), [
            RealisasiTanaman::ID_SURVEY     => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getDetailSurveyRealisasiTanaman(?)', [
            $request[RealisasiTanaman::ID_SURVEY]
        ]);

        return APIresponse(true, 'Data Survey Realisasi Tanaman Berhasil Ditemukan!', $data);
    }
}
