<?php

namespace App\Http\Controllers\v1\RealisasiTanaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\RealisasiTanaman;

class getHistorySurveyRealisasiTanaman extends Controller {
    
    function __invoke(Request $request){
        $validator = Validator::make($request->all(), [
            RealisasiTanaman::LIMIT     => 'required',
            RealisasiTanaman::OFFSET    => 'required',
            RealisasiTanaman::ID_USER   => 'required'
        ]);

        if ($validator->fails()) {
            return APIresponse(false, $validator->errors(), null, 202);
        };

        $request = $request->toArray();

        $data = DB::select('call mobile_getHistorySurveyRealisasiTanaman(?,?,?)', [
            $request[RealisasiTanaman::LIMIT],
            $request[RealisasiTanaman::OFFSET],
            $request[RealisasiTanaman::ID_USER]
        ]);

        return APIresponse(true, 'Data Survey Realisasi Tanaman Berhasil Ditemukan!', $data);
    }
}
