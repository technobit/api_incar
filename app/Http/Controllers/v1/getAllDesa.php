<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class getAllDesa extends Controller {
    
    function __invoke() {
        $data = DB::select('call getAllDesa');

        return APIresponse(true, 'Data Seluruh Desa Berhasil Ditemukan!', $data);
    }
}
