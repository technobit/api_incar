<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class getKecamatan extends Controller {
    
    function __invoke() {
        $data = DB::select('call getKecamatan');

        return APIresponse(true, 'Data Kecamatan Berhasil Ditemukan!', $data);
    }
}
