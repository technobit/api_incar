<?php

namespace App\Http\Middleware;

use Closure;

class IsAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('MToken')=="tokenincar2020") {
            return $next($request);
        }else{
            return response()->json([
                'status'    => false,
                'message'   => "Invalid Request"
            ], 503);
        }
        // return $next($request);
    }
}
