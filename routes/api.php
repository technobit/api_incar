<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/', function () {
    return [
        'app' => config('app.name'),
        'version' => '1.0.0',
    ];
});
Route::post('userLogin', 'v1\userLogin');

Route::group(array('middleware' => ['isAuthorized']), function (){

    Route::get('getAllDesa', 'v1\getAllDesa');
    Route::get('getDesa', 'v1\getDesa');
    Route::get('getKecamatan', 'v1\getKecamatan');
    Route::get('getDataJaringanIrigasi', 'v1\getDataJaringanIrigasi');
    Route::get('getJenisSaluran', 'v1\getJenisSaluran');
    Route::get('getAllSaluran', 'v1\getAllSaluran');
    Route::get('getSaluran', 'v1\getSaluran');

    Route::prefix('curahHujan')->group(function () {
        Route::get('getDetailHistorySurveyCurahHujan', 'v1\CurahHujan\getDetailHistorySurveyCurahHujan');
        Route::get('getRowCountDataSurveyCurahHujan', 'v1\CurahHujan\getRowCountDataSurveyCurahHujan');
        Route::get('getHistorySurveyCurahHujan', 'v1\CurahHujan\getHistorySurveyCurahHujan');
        Route::get('getDataStasiunCurahHujan', 'v1\CurahHujan\getDataStasiunCurahHujan');
        Route::post('insertSurveyCurahHujan', 'v1\CurahHujan\surveyCurahHujan_insert');
        Route::post('updateHistorySurveyCurahHujan', 'v1\CurahHujan\updateHistorySurveyCurahHujan');
        Route::delete('delHistorySurveyCurahHujan', 'v1\CurahHujan\delHistorySurveyCurahHujan');
        Route::get('getAllHistorySurveyCurahHujan', 'v1\CurahHujan\getAllHistorySurveyCurahHujan');
    });
    
    Route::prefix('pintuAir')->group( function() {
        Route::get('getRowCountDataSurveyPintuAir', 'v1\PintuAir\getRowCountDataSurveyPintuAir');
        Route::get('getHistorySurveyPintuAir', 'v1\PintuAir\getHistorySurveyPintuAir');
        Route::post('insertSurveyPintuAir', 'v1\PintuAir\surveypintuair_insert');
        Route::get('getDetailSurveyPintuAir', 'v1\PintuAir\getDetailSurveyPintuAir');
        Route::post('updateHistorySurveyPintuAir', 'v1\PintuAir\updateHistorySurveyPintuAir');
        Route::delete('delHistorySurveyPintuAir', 'v1\PintuAir\delHistorySurveyPintuAir');
        Route::get('getAllHistorySurveyPintuAir', 'v1\PintuAir\getAllHistorySurveyPintuAir');
    });
    
    Route::prefix('kerusakanTanaman')->group( function() {
        Route::get('getRowCountDataSurveyKerusakanTanaman', 'v1\KerusakanTanaman\getRowCountDataSurveyKerusakanTanaman');
        Route::get('getHistorySurveyKerusakanTanaman', 'v1\KerusakanTanaman\getHistorySurveyKerusakanTanaman');
        Route::get('getProgramIntensifikasi', 'v1\KerusakanTanaman\getProgramIntensifikasi');
        Route::get('getPenyebabKerusakan', 'v1\KerusakanTanaman\getPenyebabKerusakan');
        Route::get('getJenisTanaman', 'v1\KerusakanTanaman\getJenisTanaman');
        Route::post('insertSurveyKerusakanTanaman', 'v1\KerusakanTanaman\surveykerusakantanaman_insert');
        Route::get('getDetailSurveyKerusakanTanaman', 'v1\KerusakanTanaman\getDetailSurveyKerusakanTanaman');
        Route::get('getDetailSurveyJenisTanamanRusak', 'v1\KerusakanTanaman\getDetailSurveyJenisTanamanRusak');
        Route::post('updateHistoryKerusakanTanaman', 'v1\KerusakanTanaman\updateHistoryKerusakanTanaman');
        Route::delete('delHistorySurveyKerusakanTanaman', 'v1\KerusakanTanaman\delHistorySurveyKerusakanTanaman');
        Route::get('getAllHistorySurveyKerusakanTanaman', 'v1\KerusakanTanaman\getAllHistorySurveyKerusakanTanaman');
    });
    
    Route::prefix('realisasiTanaman')->group( function() {
        Route::get('getRowCountDataSurveyRealisasiTanaman', 'v1\RealisasiTanaman\getRowCountDataSurveyRealisasiTanaman');
        Route::get('getHistorySurveyRealisasiTanaman', 'v1\RealisasiTanaman\getHistorySurveyRealisasiTanaman');
        Route::post('insertSurveyRealisasiTanaman', 'v1\RealisasiTanaman\surveyrealisasitanaman_insert');
        Route::get('getDetailSurveyRealisasiTanaman', 'v1\RealisasiTanaman\getDetailSurveyRealisasiTanaman');
        Route::delete('delHistorySurveyRealisasiTanaman', 'v1\RealisasiTanaman\delHistorySurveyRealisasiTanaman');
        Route::post('updateHistorySurveyRealisasiTanaman', 'v1\RealisasiTanaman\updateHistorySurveyRealisasiTanaman');
        Route::get('getAllHistorySurveyRealisasiTanaman', 'v1\RealisasiTanaman\getAllHistorySurveyRealisasiTanaman');
    });
    
    Route::prefix('surveyBangunan')->group( function() {
        Route::get('getRowCountDataSurveyBangunan', 'v1\SurveyBangunan\getRowCountDataSurveyBangunan');
        Route::get('getHistorySurveyBangunan', 'v1\SurveyBangunan\getHistorySurveyBangunan');
        Route::get('getDataBangunan', 'v1\SurveyBangunan\getDataBangunan');
        Route::post('insertSurveyBangunan', 'v1\SurveyBangunan\surveybangunan_insert');
        Route::get('getDetailSurveyBangunan', 'v1\SurveyBangunan\getDetailSurveyBangunan');
        Route::post('updateHistorySurveyBangunan', 'v1\SurveyBangunan\updateHistorySurveyBangunan');
        Route::delete('delHistorySurveyBangunan', 'v1\SurveyBangunan\delHistorySurveyBangunan');
        Route::get('getAllHistorySurveyBangunan', 'v1\SurveyBangunan\getAllHistorySurveyBangunan');
        Route::post('uploadPhotoSurveyBangunan', 'v1\SurveyBangunan\uploadPhotoSurveyBangunan');
    });
});

