<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $method = 'AES-128-CBC';
        $key = 'key_incar';
        $iv = 'Incar_Password20';

        DB::table('m_user')->insert([
            'txtUsername'           => 'superuser',
            'txtEmail'              => 'superuser@gmail.com',
            'txtPassword'           => openssl_encrypt('superuser', $method, $key, 0, $iv),
            'txtFirstName'          => 'Super',
            'txtLastName'           => 'User',
            'txtAddress'            => 'Jl. Danau Sentani Dalam H1L4',
            'isActive'              => 1,
        ]);
    }
}
